#ifndef INCLUDED_TESTCLASS_H_
#define	INCLUDED_TESTCLASS_H_

class TestClass
{
  size_t d_importantNumber;

  public:
    TestClass();
    
    importantNumber();

  private:
    void setImportantNumber(const size_t iNum);
};

#endif
