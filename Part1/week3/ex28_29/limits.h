#ifndef INCLUDED_LIMITS_H
#define INCLUDED_LIMITS_H

#include "units.h"

struct Limits
{
    enum
    {
        ACCELERATE_HOR         = 4,             // m/s^2 horizontal acc.
        DEFAULT_GFORCE         = 2,
        MAX_GFORCE             = 9,

        BOX_LEFT               = 0,
        BOX_RIGHT              = 200000,

        DEFAULT_CLIMBRATE_FT   = 3000,

        FUEL_WARN              = 25 * 60,       // 25' flight time
        FUEL_EMPTY             = 30 * 60,       // 30' flight time

        MAX_SILENT_TIME        = 10,            // 10" silent time
                                                // (no fifoMsg was ent)
        NO_TARGET              = 0,
        TARGET_INFORM          = 1,             // see FighterStatus::checkCone
        TARGET_DELAY           = 3,             // TARGET_DELAY + alpha / 10
                                                // is the minimum target
                                                // refresh time (seconds)

        MIN_CONE_ANGLE         = 20,            // minimum (default) and max
        MAX_CONE_ANGLE         = 140,           // radar cone angles (degrees)

        TOLERANCE              = 10,            // considered same alt/dist.

        COLLISION_WARNING_TIME = 20,            // warn if collision within
                                                // this time (seconds)
        COLLISION_TIME         = 2,             // collision if within this
                                                // time

        GND_PT_FACTOR          = 0,
        STD_PT_FACTOR          = 1,
        BOX_PT_FACTOR          = 5,

        TARGET_PTS             = 1000,
        EMPTY_PENALTY          = 5000,
        COLLISION_PENALTY      = 10000,

        HEARTBEAT              = 60,            // sign of life sec.
                                                // fm the interactor

                                                // check for collisions
                                                // below this distance

        COLLISION_WARNING_DISTANCE = Units::nm2m(5),
        MAX_RADAR_RANGE            = Units::nm2m(100),
                                                // when a threat becomes visual
        VISUAL_DISTANCE            = Units::nm2m(8),
        VISUAL_ALTITUDE            = int(Units::ft2m(5000)),
        NEAR_DISTANCE              = Units::nm2m(10),
    };
    double const V_MAX               = Units::kts2m_s(700);
    double const TO_SPEED            = Units::kts2m_s(160);
    double const BOX_BASE            = Units::ft2m(3000);
    double const BOX_CEIL            = Units::ft2m(25000);

    double const DEFAULT_CLIMBRATE   = Units::ft2m(DEFAULT_CLIMBRATE_FT);

    double const MAX_CLIMBRATE       = Units::ft_min2m_s(20000);
    double const CEILING             = Units::ft_min2m_s(50000);

    double const s_earthAcceleration = 9.8;
    double const s_epsilon           = 1e-3; // tolerance of doubles considered 0
};

#endif
