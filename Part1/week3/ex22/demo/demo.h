#ifndef INCLUDED_DEMO_
#define INCLUDED_DEMO_


class Demo
{
    public:
        Demo(); // demo1.cc

        void run() &;       // run1.cc
        void run() const &; // run2.cc
        void run() &&;      // run3.cc

    private:
};

#endif
