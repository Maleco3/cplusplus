#include "main.ih"

int main(int argc, char **argv)
{
    // The direct function calls
    Demo demo;
    demo.run();
    Demo const demo2;
    demo2.run();
    Demo().run();

    // The caller functions
    caller(demo);
    caller(demo2);
    caller(Demo());



}
