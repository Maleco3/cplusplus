#ifndef INCLUDED_PRECOMP_
#define INCLUDED_PRECOMP_
#include <string>

class Precomp
{
  size_t d_value;
  std::string d_text;

  public:
    Precomp();
    Precomp(size_t value, std::string text);

    size_t value();
    std::string text();

  private:
    void set(size_t value);
    void set(std::string text);
};

#endif
