#include "memory.ih"

int Memory::load(int address)
{
  if (address >= 0 and address < static_cast<int>(RAM::SIZE) )
    return d_values[address];
  else
    return 0;
}
