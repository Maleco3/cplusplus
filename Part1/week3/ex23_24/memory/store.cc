#include "memory.ih"

void Memory::store(int address, int value)
{
  if (address >= 0 and address < static_cast<int>(RAM::SIZE) )
    d_values[address] = value;

}
