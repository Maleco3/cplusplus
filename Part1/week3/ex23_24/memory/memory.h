#ifndef INCLUDED_MEMORY_
#define INCLUDED_MEMORY_
#include "../enums/enums.h"


class Memory
{
  int d_values[static_cast<int>(RAM::SIZE)];

  public:
    Memory();

    void store(int address, int value);
    int load(int address);

};

#endif
