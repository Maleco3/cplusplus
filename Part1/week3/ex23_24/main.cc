#include "main.ih"
#include "cpu/cpu.h"

int main(int argc, char **argv)
{
  Memory mem{};
  CPU cpu(mem);
  cpu.start();

}
