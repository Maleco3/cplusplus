#include "tokenizer.ih"

int Tokenizer::value(OperandType optype, string input)
{
  switch (optype) {
    case VALUE:
    case MEMORY:
      return stoi(input);
      break;
    case REGISTER:
      // convert the lowercase letter to int value
      return stoi(input) - 'a';
      break;
    default: // 
      return 0;
    }
}
