#include "tokenizer.ih"

bool Tokenizer::token(Operand *op)
{
  string input;
  cin >> input;
  if (input[0] == '@') // case MEMORY
  {
    // pas the input string, minus the first character
    op->operandType = MEMORY;
    op->value = value(MEMORY, input.erase(0,1));
  }
  else if (input[0] >= 'a' and input[0] <= 'z') // case REGISTER
  {
    op->operandType = REGISTER;
    op->value = value(REGISTER, input);
  }
  // Try-catch to test if input is whole number
  try
  {
    op->value = stoi(input);
    op->operandType = VALUE;
  }
  catch (const std::invalid_argument& ia)
  {
    op->operandType = SYNTAX;
  }
  return op;
}
