/*
converting the first element to an Opcode, which is then returned.
*/
#include "tokenizer.ih"

Opcode Tokenizer::opcode()
{
  string input;
  cin >> input;
  switch (input[2])
  {
    case 'v':
    if (input == "mov")
    {
      return MOV;
      break;
    }
    if (input =="div")
    {
      return DIV;
      break;
    }
    case 'd':
      if (input =="add")
      {
        return ADD;
        break;
      }
    case 'b':
      if (input =="sub")
      {
        return SUB;
        break;
      }
    case 'l':
      if (input =="mul")
      {
        return MUL;
        break;
      }
    case 'g':
      if (input =="neg")
      {
        return NEG;
        break;
      }
    case 'p':
      if (input =="dsp")
      {
        return DSP;
        break;
      }
    case 'o':
      if (input =="stop")
      {
        return STOP;
        break;
      }
    default:
      return ERR;
  }
}
