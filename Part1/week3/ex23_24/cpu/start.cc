#include "cpu.ih"

void CPU::start()
{
  Opcode opcode;
  while (true)
  {
    opcode = d_tokenizer.opcode();
    switch (opcode)
    {
      case MOV:
        mov();
        break;
      case ADD:
        add();
        break;
      case SUB:
        sub();
        break;
      case MUL:
        mul();
        break;
      case DIV:
        div();
        break;
      case NEG:
        neg();
        break;
      case DSP:
        dsp();
        break;
      case STOP:
        return;
      case ERR:
        error();
        break;
    }
    d_tokenizer.reset();
  }
}
