#include "cpu.ih"

int CPU::dereference (Operand *op)
{
  switch (op->operandType) {
    case SYNTAX:
      return 0;
    case VALUE:
      return op->value;
    case REGISTER:
      return d_registers[op->value];
    case MEMORY:
      return d_memory.load(op->value);
  }
  return 0;
}
