#include "cpu.ih"

bool CPU::lvalue (Operand *operand)
{
  rvalue(operand);
  return (operand->operandType == REGISTER or operand->operandType == MEMORY);
}
