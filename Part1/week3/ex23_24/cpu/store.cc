#include "cpu.ih"

void CPU::store(Operand & operand, int value)
{
  if (operand.operandType == REGISTER)
    d_registers[operand.value] = value;
  if (operand.operandType == MEMORY)
    d_memory.store(operand.value, value);
  return;
}
