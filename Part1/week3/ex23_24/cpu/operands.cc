/*
The first step is handled by a memebr operands, receiving two Operand objects.
It retrieves the operand's values, returning true if this succeeds,
otherwise false.
Finally, the member operands, having retrieved two Operands,
performs yet another test: since both operands cannot be memory locations,
error is returned if they are both memory locations.
*/
#include "cpu.ih"

bool CPU::operands (Operand *lhOperand, Operand *rhOperand)
{
  if (!lvalue(lhOperand) or !rvalue(rhOperand))
    return false;
  return (!(lhOperand->operandType == MEMORY and rhOperand->operandType == MEMORY));
}
