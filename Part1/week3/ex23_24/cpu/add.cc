#include "cpu.ih"

void CPU::add()
{
  Operand *lhOperand{};
  int value1, value2;
  if (!twoOperands(lhOperand, &value1, &value2))
    return;
  store(*lhOperand, value1 + value2);
}
