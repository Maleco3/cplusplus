// Example program
#include <iostream>
#include <string>

using namespace std;

enum Test : char {
    EEN = 'e',
    TWEE = 't'
};


int main(int argc, char **argv)
{
  cout << argv[1] << '\n';
  cout << argv[1][0] << '\n';
  Test mode = Test(argv[1][0]);
  switch (mode)
  {
  case EEN:
      cout << "EEN\n";
  case TWEE:
      cout << "TWEE\n";
  }
}
