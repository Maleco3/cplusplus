#include <iostream>
#include <string>

int main()
{
    // Read in the string
    std::string line;
    getline(std::cin, line);
    // Strip the starting and trailing blanks (spaces)
    line = line.substr(
        line.find_first_not_of(" "),
        (line.find_first_of(" ", line.find_first_not_of(" ") ) - line.find_first_not_of(" "))
    );
    std::cout << '`' << line << "'\n";
}
