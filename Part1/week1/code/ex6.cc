#include <iostream>
#include <sstream>

int main(int argc, char *argv[])
{
    // Read input arguments and convert to integers
    std::istringstream istr(argv[1]);
    int radix;
    istr >> radix;
    std::istringstream istr2(argv[2]);
    int value;
    istr2 >> value;

    int digit;
    std::string answer;
    std::cout << value << ", displayed using radix " << radix << " is: ";
    while (value != 0)
    {
        digit = value % radix;
        // Convert the ASCII value of the next digit or letter to a character
        char numberChar = (digit > 9) ? 'a' + (digit - 10) : '0' + digit;
        answer.insert(0, 1, numberChar); // Insert it into the answer
        value /= radix;
    }
    std::cout << answer << "\n";
}
