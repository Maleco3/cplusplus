#include <iostream>

enum AnalogClock 
{
    NR_OF_HALF_HOURS_IN_HOUR = 2,
    NR_OF_HALF_HOURS = 24,
    NR_OF_DEGREES = 360,
};

int main()
{
    while (1)
    {
        int inputDegrees;
        std::cout << "? ";
        std::cin >> inputDegrees;
        if (inputDegrees == 0) return 0;
        else
        {
            int halfHourCount;
            //Devision by 2 so 50% of the range is on either side
            int offset = NR_OF_DEGREES / NR_OF_HALF_HOURS / 2;
            halfHourCount = (inputDegrees + offset) / (NR_OF_DEGREES / NR_OF_HALF_HOURS);
            int hour;
            hour = halfHourCount / NR_OF_HALF_HOURS_IN_HOUR;
            bool half_hour;
            half_hour = halfHourCount % NR_OF_HALF_HOURS_IN_HOUR;
            std::cout << "object at your " << (hour == 0 ? 12 : hour) << ":" 
                << (half_hour ? "30" : "00") << " position\n";
        }
    }
}
