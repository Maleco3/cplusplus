#include <iostream>
#include <cctype>

enum States
{
    SPACE,
    WORD,
};


int main(int argc, char **argv)
{

    std::string inputparameter;
    inputparameter = argv[1];

    switch (inputparameter[0])
    {
        case 'b':
        {
            char ch;
            size_t byteCount = 0;
            while (std::cin.get(ch)) ++byteCount;
            std::cout << byteCount << " bytes\n";
            break;
        }
        case 'w':
        {
            char ch;
            size_t wordCount = 0;
            size_t state = SPACE;
            while (std::cin.get(ch))
            {
                if (state == WORD and isspace(ch)) state = SPACE;
                else if (state == SPACE and !isspace(ch)) 
                {
                    state = WORD;
                    ++wordCount;
                }
            }
            std::cout << wordCount << " words\n";
            break;
        }
        case 'l':
         {
            char ch;
            size_t lineCount = 0;
            while (std::cin.get(ch))
            {
                if (ch == '\n') ++lineCount;
            }
            std::cout << lineCount << " lines\n";
            break;
        }
        default:
            std::cout << "Invalid argument given\n";
            break;
    }
}
