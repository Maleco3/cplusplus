#include <iostream>

enum States
{
    UNKNOWN,
    ERROR,
    OK,
};

int main()
{
    size_t variable = 0;
    States state;

    while (true)
    {
        std::cout << "> ";

        // First read in the word, if not ret, also read the input value
        std::string instruction;
        std::cin >> instruction;
        if (instruction == "ret")
            break;

        int inputValue;
        std::cin >> inputValue;

        // The state is UNKNOWN if the word does not match a command.
        // In case of a zero division, an ERROR is given.
        // In all other cases (so successful ones), the state is OK.
        state = OK;
        switch (instruction[0])
        {
            case 's':
                if (instruction == "sto")
                    variable = inputValue;
                else if (instruction == "sub")
                    variable -= inputValue;
                else
                    state = UNKNOWN;
                break;
            case 'a':
                instruction == "add" ? variable += inputValue : state = UNKNOWN;
                break;
            case 'm':
                instruction == "mul" ? variable *= inputValue : state = UNKNOWN;
                break;
            case 'd':
                if (instruction == "div")
                    inputValue != 0 ? variable /= inputValue : state = ERROR;
                else
                    state = UNKNOWN;
                break;
            default:
                state = UNKNOWN;
                break;
        }

        // Give a message to the user, depending on the state of the system
        switch (state)
        {
            case ERROR:
                std::cout << "ERROR: Zero division, please don't...\n";
                break;
            case OK:
                std::cout << "OK! Variable = " << variable << "\n";
                break;
            default:
                std::cout << "State is UNKNOWN, please enter a valid command...\n";
                break;
        }
    }
}
