#include "main.ih"

extern char **environ;

int main(int argc, char **argv)
{

  Strings clArguments = Strings(argc, argv);
  Strings envContents = Strings(environ);

  if (envContents.size() >= 4)
    clArguments.add(envContents.at(4));
  size_t length = clArguments.size();
  for (size_t idx = 0; idx != length; ++idx)
    cout << clArguments.at(idx) << '\n';

  Strings object = Strings(argc, argv);
  Strings const constObject = Strings(argc, argv);
  cout << object.at(0) << '\n';       // Calls the non-const at member function
  cout << constObject.at(0) << '\n';  // Calls the const at member function

  cout << "left: " << clArguments.at(1) << '\n';       // Calls the non-const at member function
  cout << "right: " << envContents.at(1) << '\n';       // Calls the non-const at member function
  Strings::stringSwap(clArguments, envContents);
  cout << "left: " << clArguments.at(1) << '\n';       // Calls the non-const at member function
  cout << "right: " << envContents.at(1) << '\n';       // Calls the non-const at member function

}
