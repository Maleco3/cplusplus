#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_
#include <string>


class Strings
{
  std::string *d_str;
  size_t d_size;

  public:
    Strings(int argc, char *argv[]);
    Strings(char **environ);

    std::string       &at(size_t index);
    std::string const &at(size_t index) const;
    size_t size() const;

    void add(std::string string); // add1
    void add(char *cstring); // add2
    static void stringSwap(Strings &left, Strings &right);

  private:
    void copy(std::string *one, std::string *two, size_t size);
};

#endif
