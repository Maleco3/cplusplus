#include "strings.ih"

void Strings::stringSwap(Strings &left, Strings &right)
{
  Strings tmp = left;
  left = right;
  right = tmp;
}
