#include "strings.ih"

Strings::Strings(char **environ)
//:
{
  size_t count = 0;
  for (char *s = *environ; s; ++count)
    s = *(environ + count);

  d_size = count - 1;
  d_str = new string[d_size];

  for (int idx = d_size; idx--; )
    d_str[idx] = environ[idx];
}
