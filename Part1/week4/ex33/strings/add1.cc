#include "strings.ih"

void Strings::add(string str)
{
  // copy the existing contents to a new storage area
  string *tmp = new string[d_size + 1];
  for (size_t idx = 0; idx !=  d_size; ++idx)
    tmp[idx] = d_str[idx];
  // add the next string to the new storage area
  tmp[d_size] = str;

  // destroy the old information
  delete[] d_str;

// update d_str and d_size so that they refer to the new storage area.
  d_str = tmp;
  ++d_size;
}
