// Example program
#include <iostream>
#include <string>

using namespace std;

int main(int argc, char **argv)
{
    // int x[8];
    // for (int idx = 7; idx--; )
    //     x[idx] = idx;
    //
    // *(x + 2) = *(x + 3);
    // cout << *(x)  << endl;
    // cout << *(x + 1)  << endl;
    // cout << *(x + 2)  << endl;
    // cout << *(x + 3)  << endl;
    // cout << *(x + 4)  << endl;
    // cout << *(x + 5)  << endl;
    // cout << *(x + 6)  << endl;
    // cout << *(x + 7)  << endl;
    //
    // cout << &x[10] << endl;
    // cout << &*(x + 10) << endl;
    // cout << endl;
    // cout << "1 " << argv[0] << endl;
    // cout << "2 " << (argv[0]++) << endl;
    // cout << "3 " << (argv++[0]) << endl;
    // cout << "4 " << (++argv[0]) << endl;
    // cout << endl;
    // cout << argv[0] << endl;
    // cout << argv[1] << endl;
    // cout << argv[2] << endl;
    //
    string x = "test", y = "test2";
    size_t size = 2;
    string *sp = new string[size];
    sp[0] = x;
    *(sp + 1) = y;
    cout << &x << " " << x << endl;
    cout << &y << " " << y << endl;
    cout << sp << " " << *sp << endl;
    cout << sp + 1 << " " << *(sp+1) << endl;
}
