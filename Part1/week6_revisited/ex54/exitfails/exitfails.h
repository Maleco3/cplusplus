#ifndef INCLUDED_EXITFAILS_
#define INCLUDED_EXITFAILS_

#include <iosfwd>
#include <string>

struct acct_v3;

class Exitfails
{
    bool d_printall;
    std::string d_filename;

    public:
        Exitfails();                        // 1
        Exitfails(int argc, char **argv);   // 2

        void run();
    private:
        void parseInput(int argc, char **argv);
        void readFile(std::ifstream &file);
        void readStruct(std::ifstream &file, acct_v3 *st);
        void report(acct_v3 &acctBlock) const;
};

#endif
