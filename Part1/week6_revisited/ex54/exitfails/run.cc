#include "exitfails.ih"

void Exitfails::run()
{
    ifstream file(d_filename, ios::in | ios::binary);
    if (file.is_open())
        readFile(file);
    else
        cout << "Could not open file\n";
}
