#include "exitfails.ih"

void Exitfails::readStruct(ifstream &file, acct_v3 *st)
{
    file.read(reinterpret_cast<char *>(st), sizeof(*st));
}
