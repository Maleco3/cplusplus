#include "exitfails.ih"

string exitcodemanip(size_t exitcode)
{
  switch(exitcode)
  {
      case SIGKILL:
          return "SIGKILL";
      case SIGTERM:
          return "SIGTERM";
      default:
          return to_string(exitcode);
  }
}

void Exitfails::report(acct_v3 &acctBlock) const
{
    if (acctBlock.ac_exitcode != 0 or d_printall)
    {
        cout << '\'' << acctBlock.ac_comm << "'\t"
             << exitcodemanip(acctBlock.ac_exitcode)
             << '\n';
    }
}
