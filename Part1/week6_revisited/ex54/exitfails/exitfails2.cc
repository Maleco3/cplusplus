#include "exitfails.ih"

Exitfails::Exitfails(int argc, char **argv)
:
    Exitfails()
{
    if (argc > 1)
        parseInput(argc, argv);
}
