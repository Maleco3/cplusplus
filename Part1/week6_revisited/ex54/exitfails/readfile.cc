#include "exitfails.ih"

void Exitfails::readFile(ifstream &file)
{
    acct_v3 acctBlock;
    while(!file.eof())
    {
        readStruct(file, &acctBlock);
        report(acctBlock);
    }
}
