#include "dna.ih"

void DNA::resetFile(ifstream &file)
{
  file.clear();
  file.seekg(0, ios::beg);
}
