#include "dna.ih"

void DNA::parseInput(int argc, char **argv)
{
    switch(argc)
    {
        case 3:
            d_filenameIn = argv[1];
            d_filenameOut = argv[2];
            break;
        case 4:
            if(string(argv[1]) != "-b")
                usage(argv[0]);
            d_binaryOut = true;
            d_filenameIn = argv[2];
            d_filenameOut = argv[3];
            break;
        default:
            usage(argv[0]);
    }
}
