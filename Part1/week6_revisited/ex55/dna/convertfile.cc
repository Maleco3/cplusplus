#include "dna.ih"

void DNA::convertFile(ifstream &inFile)
{
    ofstream outFile(d_filenameOut, ios::out | ios::trunc | ios::binary);
    (d_binaryIn == d_binaryOut) ? copyStream(inFile, outFile) :
                     d_binaryIn ? binToHum(inFile, outFile) :
                                  humToBin(inFile, outFile);
}
