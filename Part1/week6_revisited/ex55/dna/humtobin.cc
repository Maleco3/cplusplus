#include "dna.ih"

void DNA::humToBin(std::ifstream &inFile, std::ofstream &outFile)
{
  long nucCounter = 0;    // Write a long (large enough for the human genome)
                          // to the start of the binary
                          // file. This will later contain the number of
                          // nucleotides in the file.
  outFile.write(reinterpret_cast<char *>(&nucCounter), sizeof(nucCounter));

  char in;
  while(!inFile.eof() )
  {
    char outByte = 0;           // The byte we are going to write
    for (int bit = (charLength - letterBits) ; bit >= 0; bit -= letterBits)
    {
      if (inFile.read(&in, sizeof(in)))
      {
        switch (in)
        {
          case 'A':
          outByte |= (Abin << bit);
          break;
          case 'T':
          outByte |= (Tbin << bit);
          break;
          case 'G':
          outByte |= (Gbin << bit);
          break;
          case 'C':
          outByte |= (Cbin << bit);
          break;
        }
        ++nucCounter;
      }
    }
    outFile << char(outByte);
  }
  // Insert the actual number of nucleotides at the start of the file
  outFile.seekp(0);
  outFile.write(reinterpret_cast<char *>(&nucCounter), sizeof(nucCounter));
}
