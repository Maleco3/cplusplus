#ifndef INCLUDED_DNA_
#define INCLUDED_DNA_

#include <iosfwd>
#include <string>

class DNA
{
    bool d_binaryIn, d_binaryOut;
    std::string d_filenameIn;
    std::string d_filenameOut;

    public:
        DNA(int argc, char **argv);

        void run();

    private:
        void binToHum(std::ifstream &inFile, std::ofstream &outfile);
        bool checkChar(char check);
        void checkFile(std::ifstream &inFile);
        void convertFile(std::ifstream &inFile);
        void copyStream(std::ifstream &inFile, std::ofstream &outFile);
        void humToBin(std::ifstream &inFile, std::ofstream &outFile);
        void parseInput(int argc, char **argv);
        void resetFile(std::ifstream &inFile);
        void usage(std::string const &progname) const;
};

#endif
