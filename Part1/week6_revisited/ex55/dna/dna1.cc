#include "dna.ih"

DNA::DNA(int argc, char **argv)
:
    d_binaryIn(false),
    d_binaryOut(false)
{
    parseInput(argc, argv);
}
