#include "dna.ih"

void DNA::usage(std::string const &progname) const
{
    cout << "\n" <<
    "Usage: " << progname << " [-b] fileIn fileOut\n"
    "Where:\n"
    "   -b      - optional argument to write binary output\n"
    "   fileIn  - Filename of the input file\n"
    "   fileOut - Filename of the output file\n"
    "\n";
    exit(1);
}
