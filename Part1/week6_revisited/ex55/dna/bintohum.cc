#include "dna.ih"

void DNA::binToHum(ifstream &inFile, ofstream &outFile)
{
  long nucCounter;
  inFile.read(reinterpret_cast<char *>(&nucCounter), sizeof(nucCounter));

  char in;
  for (long idx = 0; idx != nucCounter && !inFile.eof(); ++idx)
  {
    if (idx % 4 == 0)
      inFile.read(&in, sizeof(in));

    if((in & Cmask) == Cmask)
      outFile << 'C';
    else if((in & Gmask) == Gmask)
      outFile << 'G';
    else if((in & Tmask) == Tmask)
      outFile << 'T';
    else if((in & Amask) == Amask)
      outFile << 'A';
    in <<= letterBits;
  }
}
