#include "dna.ih"

void DNA::run()
{
    ifstream file(d_filenameIn, ios::in | ios::binary);
    if (file.is_open())
    {
        checkFile(file);
        resetFile(file);
        convertFile(file);
    }
    else
        cout << "Could not open file\n";
}
