#include "main.ih"

ostream &width(ostream &stream)
{
    stream.width(15);
    return stream;
}

ostream &endline(ostream &stream)
{
  stream.copyfmt(ios(NULL));  // Reset the flags
  stream << '\n';             // Insert trailing newline
  return stream;
}

int main(int argc, char **argv)
{
    double value = 12.04;

    cout << width << value << endline
         << width << left << value << endline
         << width << right << value << endline
         << width << setprecision(1) << fixed << value << endline
         << width << setprecision(4) << fixed << value << endline
         << width << value << endline;
}
