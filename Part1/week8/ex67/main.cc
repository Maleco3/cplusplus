#include "main.ih"

void printMatrix(Matrix test, size_t dim)
{
  for (size_t row = 0; row != dim; ++row)
  {
    double *row1 = test.row(row);
    for (size_t idx = 0; idx != dim; ++idx)
    cout << (int)*(row1 + idx) << ' ';
    cout << '\n';
  }
}

int main(int argc, char **argv)
{
  Matrix mat1(4, 4);
  cin >> mat1(2, 2, Matrix::BY_ROWS);
  cout << mat1;
}
