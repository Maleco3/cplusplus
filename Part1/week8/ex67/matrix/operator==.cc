#include "matrix.ih"

bool Matrix::operator==(Matrix const &other) const
{
  return (d_nRows == other.d_nRows and  // Memcmp returns 0 if memory is equal,
          d_nCols == other.d_nCols and  // therefore the negation.
          !memcmp(d_data, other.d_data, size() * sizeof(double)));
}
