#include "matrix.ih"

void extract(istream &lvalue, Matrix &rvalue)
{
  size_t end1;
  size_t end2;

  if (rvalue.d_option == rvalue.BY_COLS)
  {
      cout << "setting stuff";
    end1 = rvalue.d_extractNumber ?
                    min(rvalue.d_start + rvalue.d_extractNumber, rvalue.d_nCols)
                    : rvalue.d_nCols;
    end2 = rvalue.d_nRows;
  }
  else
  {
    end1 = rvalue.d_extractNumber ?
                min(rvalue.d_start + rvalue.d_extractNumber, rvalue.d_nRows)
              : rvalue.d_nRows;
    end2 = rvalue.d_nCols;
  }

  for (size_t idx1 = rvalue.d_start; idx1 < end1; ++idx1)
  {
    for (size_t idx2 = 0; idx2 < end2; ++idx2)
      rvalue.d_option == rvalue.BY_COLS ? lvalue >> rvalue[idx2][idx1]
                                        : lvalue >> rvalue[idx1][idx2];
  }
}
