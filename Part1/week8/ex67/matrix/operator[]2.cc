#include "matrix.ih"

double *Matrix::operator[](size_t idx) const
{
  return d_data + idx * d_nCols;
}
