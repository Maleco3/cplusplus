#include "matrix.ih"

Matrix &Matrix::operator()(long option, size_t start, size_t extractNumber)
{
  d_option = option;
  d_start = start;
  d_extractNumber = extractNumber;
  return *this;
}
