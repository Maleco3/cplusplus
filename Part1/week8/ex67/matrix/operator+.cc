#include "matrix.ih"

Matrix &operator+(Matrix &lhs, Matrix const &rhs)
{
  lhs += rhs;
  return lhs;
}
