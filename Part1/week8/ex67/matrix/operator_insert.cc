#include "matrix.ih"

ostream &operator<<(ostream &out, Matrix const &mat)
{

  size_t nRows = mat.nRows(), nCols = mat.nCols();
  for (size_t row = 0; row != nRows; ++row)
    for (size_t col= 0; col!= nCols; ++col)
      out << mat[row][col] << (col < (nCols - 1) ? '\t' : '\n');
  return out;
}
