#include "matrix.ih"

Matrix &Matrix::operator+=(Matrix const &rhs)
{
  if (!(d_nRows == rhs.nRows()) or !(d_nCols == rhs.nCols()))
    exit(1);

  for (size_t row = 0; row != d_nRows; ++row)
    for (size_t col = 0; col != d_nCols; ++col)
    {
      *(d_data + row * d_nCols + col) += rhs[row][col];
    }
  return *this;
}
