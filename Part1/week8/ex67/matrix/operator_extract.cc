#include "matrix.ih"

void operator>>(istream &lvalue, Matrix &rvalue)
{
  if ((rvalue.d_option == rvalue.BY_COLS && rvalue.d_start < rvalue.d_nCols) ||
     (rvalue.d_option == rvalue.BY_ROWS && rvalue.d_start < rvalue.d_nRows))
    extract(lvalue, rvalue);
}
