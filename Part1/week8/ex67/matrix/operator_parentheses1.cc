#include "matrix.ih"

Matrix &Matrix::operator()(size_t nRows, size_t nCols, long option)
{
  Matrix tmp (nRows, nCols);
  swap(tmp);
  d_option = option;
  return *this;
}
