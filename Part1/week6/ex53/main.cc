#include "main.ih"

ostream &width(ostream &stream)
{
    stream.width(15);
    return stream;
}

int main(int argc, char **argv)
{
    double value = 12.04;
    streamsize s = cout.precision();

    cout << width << value << '\n'
         << width << left << value << '\n'
         << width << right << value << "\n"
         << width << setprecision(1) << fixed << value << "\n"
         << width << setprecision(4) << fixed << value << "\n"
         << width << setprecision(s) << defaultfloat << value << "\n";
}
