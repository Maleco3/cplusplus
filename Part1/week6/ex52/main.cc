#include <iostream>
#include <ctime>
#include <string>

using namespace std;

ostream &now(ostream &stream)
{
    time_t tt {time(0)};
    string str = asctime(localtime(&tt));
    str.erase(str.end() - 1); // remove the trailing newline
    return stream << str;
}

int main(int argc, char **argv)
{
    cout << now << '\n';
}
