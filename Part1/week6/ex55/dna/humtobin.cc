#include "dna.ih"

void DNA::humToBin(std::ifstream &inFile, std::ofstream &outFile)
{
  int bitCounter = 0, out = 0;
  char in;
  while(!inFile.eof())
  {
    inFile.read(&in, sizeof(in));
    switch (in)
    {
      case 'A':
        out |= (Abin << bitCounter);
        break;
      case 'T':
        out |= (Tbin << bitCounter);
        break;
      case 'G':
        out |= (Gbin << bitCounter);
        break;
      case 'C':
        out |= (Cbin << bitCounter);
        break;
    }
    if (!bitCounter)
    {
      outFile << char(out);
      out = 0;
    }
    bitCounter = (bitCounter + letterBits) % charLength;
  }
}
