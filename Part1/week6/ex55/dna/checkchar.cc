#include "dna.ih"

bool DNA::checkChar(char check)
{
  switch(check)
  {
      case 'A':
      case 'T':
      case 'C':
      case 'G':
      case ' ':
      case '\n':
        return true;
  }
  return false;
}
