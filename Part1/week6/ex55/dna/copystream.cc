#include "dna.ih"

void DNA::copyStream(ifstream &inFile, ofstream &outFile)
{
    char in;
    while(!inFile.eof())
    {
        inFile.read(&in, sizeof(in));
        outFile << in;
    }
}
