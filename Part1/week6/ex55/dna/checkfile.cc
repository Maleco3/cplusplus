#include "dna.ih"

void DNA::checkFile(ifstream &file)
{
    char check;
    while(!file.eof())
    {
        file.read(&check, sizeof(check));
        if(!checkChar(check))
        {
            d_binaryIn = true;
            return;
        }
    }
}
