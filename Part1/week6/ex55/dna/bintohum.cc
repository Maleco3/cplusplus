#include "dna.ih"

void DNA::binToHum(ifstream &inFile, ofstream &outFile)
{
  char in;
  while(!inFile.eof())
  {
    inFile.read(&in, sizeof(in));
    while(in)
    {
      if((in & Cmask) == Cmask)
        outFile << 'C';
      else if((in & Gmask) == Gmask)
        outFile << 'G';
      else if((in & Tmask) == Tmask)
        outFile << 'T';
      else if((in & Amask) == Amask)
        outFile << 'A';
      in <<= letterBits;
    }
  }
}
