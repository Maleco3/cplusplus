#include "exitfails.ih"

void Exitfails::readStruct(ifstream &file, acct_v3 *st)
{
    file.read(reinterpret_cast<char *>(&st->ac_flag), sizeof(st->ac_flag));
    file.read(reinterpret_cast<char *>(&st->ac_version), sizeof(st->ac_version));
    file.read(reinterpret_cast<char *>(&st->ac_tty), sizeof(st->ac_tty));
    file.read(reinterpret_cast<char *>(&st->ac_exitcode), sizeof(st->ac_exitcode));
    file.read(reinterpret_cast<char *>(&st->ac_uid), sizeof(st->ac_uid));
    file.read(reinterpret_cast<char *>(&st->ac_gid), sizeof(st->ac_gid));
    file.read(reinterpret_cast<char *>(&st->ac_pid), sizeof(st->ac_pid));
    file.read(reinterpret_cast<char *>(&st->ac_ppid), sizeof(st->ac_ppid));
    file.read(reinterpret_cast<char *>(&st->ac_btime), sizeof(st->ac_btime));
    file.read(reinterpret_cast<char *>(&st->ac_etime), sizeof(st->ac_etime));
    file.read(reinterpret_cast<char *>(&st->ac_utime), sizeof(st->ac_utime));
    file.read(reinterpret_cast<char *>(&st->ac_stime), sizeof(st->ac_stime));
    file.read(reinterpret_cast<char *>(&st->ac_mem), sizeof(st->ac_mem));
    file.read(reinterpret_cast<char *>(&st->ac_io), sizeof(st->ac_io));
    file.read(reinterpret_cast<char *>(&st->ac_rw), sizeof(st->ac_rw));
    file.read(reinterpret_cast<char *>(&st->ac_minflt), sizeof(st->ac_minflt));
    file.read(reinterpret_cast<char *>(&st->ac_majflt), sizeof(st->ac_majflt));
    file.read(reinterpret_cast<char *>(&st->ac_swaps), sizeof(st->ac_swaps));
    file.read(reinterpret_cast<char *>(&st->ac_comm), sizeof(st->ac_comm));
}
