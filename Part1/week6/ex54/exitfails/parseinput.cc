#include "exitfails.ih"

void Exitfails::parseInput(int argc, char **argv)
{
    for (int idx= 1; idx != argc; ++idx)
    {
        switch(argv[idx][0]) {
            case '-':
                d_printall = string(argv[idx]) == "-a" ? true : false;
                break;
            default:
                d_filename = argv[idx];
                break;
        }
    }
}
