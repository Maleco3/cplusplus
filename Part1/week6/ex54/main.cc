#include "main.ih"

int main(int argc, char **argv)
{
    Exitfails ex(argc, argv);
    ex.run();
}
