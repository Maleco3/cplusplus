#include "main.ih"

int main(int argc, char **argv)
{
    // For loop for the number of possible combinations, 2^(argc-1)
    size_t end = 1 << (argc - 1);
    for (size_t idx = 0; idx != end; ++idx)
    {
        // Add 1 to start counting from 1, not 0
        cout << idx + 1 << ": ";
        // For loop for the number of arguments (minus the filename)
        for (int argNum = 0; argNum < argc - 1; ++argNum)
            // get 2^argNum for binary comparison, 1 = 2^0
            if (idx & 1 << argNum)
                cout << argv[argNum + 1] << ' ';
        cout << '\n';
    }
}
