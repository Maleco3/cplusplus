#include "main.ih"

enum Mode
{
    BYTES = 'b',
    LINES = 'l',
    WORDS = 'w',
    ERROR
};

enum States
{
    SPACE,
    WORD,
};

int main(int argc, char **argv)
{
    Mode mode = Mode(argv[1][0]);
    char ch;

    switch (mode)
    {
    case BYTES:
    case LINES:
        {
            size_t byteCount = 0, lineCount = 0;
            while (cin.get(ch))
            {
                ++byteCount;
                if (ch == '\n')
                    ++ lineCount;
            }
            if (mode == BYTES)
                cout << byteCount << " bytes\n";
            else
                cout << lineCount << " lines\n";
            break;
        }
    case WORDS:
        {
            size_t wordCount = 0;
            States state = SPACE;
            while (cin.get(ch))
            {
                if (state == WORD and isspace(ch))
                    state = SPACE;
                else if (state == SPACE and !isspace(ch))
                {
                    state = WORD;
                    ++wordCount;
                }
            }
            cout << wordCount << " words\n";
            break;
        }
    default:
        cout << "Invalid argument given\n";
        break;
    }

}
