#include "main.ih"

int main(int argc, char *argv[])
{
  float hdg1 = stof(argv[1]) * M_PIl / 180;
  float hdg2 = stof(argv[2]) * M_PIl / 180;
  float hdg = fmod( (hdg2 - hdg1 + 2 * M_PIl), (2 * M_PIl) );
  int yCoord = stoi(argv[3]);

  // Collision test inside the cout statement
  cout << "The planes are on " << (
  (hdg == 0) ? "parrallel" :
      ((yCoord * (hdg - M_PIl)) < 0) ? "diverting" : "converting"
  ) << " tracks" << '\n';
}
