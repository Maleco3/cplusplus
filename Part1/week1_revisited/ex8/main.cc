#include "main.ih"

enum AnalogClock
{
    NR_OF_HALF_HOURS_IN_HOUR = 2,
    NR_OF_HALF_HOURS = 24,
    NR_OF_DEGREES = 360,
};

int main(int argc, char **argv)
{
    size_t offset = NR_OF_DEGREES / NR_OF_HALF_HOURS / 2;

    while (true)
    {
        cout << "? ";
        size_t inputDegrees;
        cin >> inputDegrees;
        if (inputDegrees == 0)
            return 0;

        size_t halfHourCount = (inputDegrees + offset) / (NR_OF_DEGREES / NR_OF_HALF_HOURS);
        size_t hour = halfHourCount / NR_OF_HALF_HOURS_IN_HOUR;
        bool half_hour = halfHourCount % NR_OF_HALF_HOURS_IN_HOUR;
        cout << "object at your " << (hour == 0 ? 12 : hour) << ":"
        << (half_hour ? "30" : "00") << " position\n";
    }
}
