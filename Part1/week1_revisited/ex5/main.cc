#include "main.ih"

int main()
{
    enum States
    {
        UNKNOWN,
        ERROR,
        OK,
    };

    size_t variable = 0;
    States state = UNKNOWN;

    while (true)
    {
        cout << "> ";

        // First read in the word, if not ret, also read the input value
        string instruction;
        cin >> instruction;
        if (instruction == "ret")
        return 0;

        int inputValue;
        cin >> inputValue;

        switch (instruction[2])
        {
        case 'o': // Store the value
            if (instruction == "sto")
            {
                variable = inputValue;
                state = OK;
                break;
            }
        case 'd': // Addition
            if (instruction == "add")
            {
                variable += inputValue;
                break;
            }
        case 'b': // Subtraction
            if (instruction == "sub")
            {
                variable -= inputValue;
                break;
            }
        case 'l': // Multiplication
            if (instruction == "mul")
            {
                variable *= inputValue;
                break;
            }
        case 'v': // Division
            if (instruction == "div")
            {
                if (inputValue == 0)
                    state = ERROR;
                else
                    variable /= inputValue;
                break;
            }
        default:
            state = UNKNOWN;
            break;
        }

        // Give a message to the user, depending on the state of the system
        if (state == ERROR)
        {
            cout << "ERROR: Zero division, please don't..." << '\n';
            state = OK;
        } else
            cout << "variable: " << variable << '\n';
    }
}
