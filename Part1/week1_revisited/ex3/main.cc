#include "main.ih"

int main(int argc, char **argv)
{
    // Read in the string
    string line;
    getline(cin, line);
    // Strip the starting and trailing blanks (spaces)
    line
        // Erase from 1 position beyond the last character to end
        .erase(line.find_last_not_of(' ')+1)
        // Erase from start until no more spaces are found
        .erase(0, line.find_first_not_of(' '));
    cout << '`' << line << "'" << '\n';
}
