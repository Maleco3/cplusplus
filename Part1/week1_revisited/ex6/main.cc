#include "main.ih"

int main(int argc, char *argv[])
{
    // Read input arguments and convert to integers
    size_t radix = stoi(argv[1]);
    size_t value = stoi(argv[2]);

    string answer;
    while (value != 0)
    {
        size_t digit = value % radix;
        // Convert the ASCII value of the next digit or letter to a character
        answer.insert(0, 1, (digit > 9 ? 'a' + (digit - 10) : '0' + digit));
        value /= radix;
    }
    cout << argv[2] << ", displayed using radix " << radix << " is: " << answer << '\n';
}
