// Called when the capacity must be equal to the size in d_capacity
#include "strings.ih"

void Strings::reserve()
{
  string **newMemory = rawPointers(d_capacity); // Create new block of memory

  memcpy(newMemory, d_str, d_size*sizeof(string *));

  destroy();
  d_str = newMemory;
}
