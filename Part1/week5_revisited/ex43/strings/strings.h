#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_
#include <iosfwd>


class Strings
{
  size_t d_size{0};                             // Number of pointers in use
  size_t d_capacity{1};                         // Capacity of the class
  std::string **d_str{rawPointers(d_capacity)};

  public:
    Strings(int argc, char *argv[]);  //1
    Strings(char **environ);          //2
    ~Strings();

    std::string       &at(size_t index);
    std::string const &at(size_t index) const;
    size_t size() const;

    void add(std::string string); // add1
    void stringSwap(Strings &other);
    void destroy();

    std::size_t capacity() const;
    void resize(size_t size);
    void reserve(size_t size); // 1, figure out the steps needed to resize

  private:
    size_t countVar(char **environ);  // Count the number of variables
    std::string **rawPointers(size_t size);
    void reserve();             // 2, reserve the actual size
};

#endif
