#include "strings.ih"

void Strings::resize(size_t size)
{
  if (size >= d_capacity)
  {
    reserve(size);                                // Reserve the capacity
    for (size_t idx = d_size; idx != size; ++idx) // Fill with empy strings
    {
      d_str[idx] = new string();
      d_size = size;                              // update size value
    }
  }
  else
    for (size_t idx = size; idx != d_size; ++idx)
      d_str[idx] = nullptr;   // Reset the pointer to null
}
