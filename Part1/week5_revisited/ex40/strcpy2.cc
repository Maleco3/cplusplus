#include "main.ih"

void strcpy2(char dst[], char const src[])
{
  size_t idx = 0;
  while (src[idx])
  {
    dst[idx] = src[idx];
    ++idx;
  }
}
