#include "strings.ih"

Strings::Strings(int argc, char *argv[])
:
  d_size(argc),
  d_str(new string[d_size])
{
  for (int idx = d_size; idx--; )
    d_str[idx] = argv[idx];
}
