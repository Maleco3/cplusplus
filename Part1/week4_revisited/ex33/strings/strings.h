#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <iosfwd> // For the use of string/size_t type

class Strings
{
  size_t d_size;
  std::string *d_str;

  public:
    Strings(int argc, char *argv[]);
    Strings(char **environ);

    std::string       &at(size_t index);
    std::string const &at(size_t index) const;
    size_t size() const;

    void add(std::string const string); // add1
    void add(char const *cstring);      // add2

    void stringSwap(Strings &other);

  private:
    void copy(std::string *one, std::string *two, size_t size);
    size_t countVar(char **environ);  // Count the number of variables
};

#endif
