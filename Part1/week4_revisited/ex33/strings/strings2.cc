#include "strings.ih"

Strings::Strings(char **environ)
:
  d_size(countVar(environ)),
  d_str(new string[d_size])
{
  for (int idx = d_size; idx--; )
    d_str[idx] = environ[idx];
}
