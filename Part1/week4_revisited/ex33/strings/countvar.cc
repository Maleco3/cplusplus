#include "strings.ih"

size_t Strings::countVar(char **environ)
{
  size_t count = 0;
  char *cp;
  while ((cp = environ[count]))
    ++count;
  return count;
}
