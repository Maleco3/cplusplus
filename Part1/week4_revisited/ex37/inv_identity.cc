#include "main.ih"

void inv_identity(int *data[DIMCOL])
{
  for (int *row = data[0]; row - data[0] != DIMROW*DIMCOL; row += DIMROW)
    for (int *col = &row[0], *ident = &row[(row - data[0]) / DIMROW];
         col - &row[0] != DIMCOL; ++col)
      *col = (col == ident) ? 0 : 1;
}
