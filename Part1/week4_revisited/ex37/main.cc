#include "main.ih"

int main(int argc, char **argv)
{
  int square[DIMROW][DIMCOL];
  int *row[DIMROW];
  for (size_t idx = DIMROW; idx--;)
    row[idx] = &square[idx][0];

  inv_identity(row);
}
