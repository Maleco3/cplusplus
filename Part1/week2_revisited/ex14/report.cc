#include "main.ih"

void report(Vars const &vars)
{
  float qualityScore = min(
    (float)100,
    2 * (((float)vars.commentScore)/ vars.lineScore) * 100
  );
  cout << "Number of lines:\n\t" << vars.lineCount << '\n'
       << "Number of eoln comments:\n\t" << vars.eolnCount << '\n'
       << "Number of C-style comments:\n\t" << vars.cStyleCount << '\n'
       << "Maximum number of nesting:\n\t" << vars.maxNested << '\n'
       << "Documentation quality score:\n\t" << qualityScore << '%' << '\n';
}
