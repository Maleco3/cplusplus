#include "main.ih"

void processline(string const &line, Vars &vars)
{
  for (auto it = line.begin(); it != line.end(); ++it)
  {
    // Check if we are still in a c-style comment
    if (vars.IN_C_COMMENT)
    {
      if (*it == '*' and *(it + 1) == '/')
      updateVars(END_C_STYLE, vars);
    }
    else
    {
      switch (*it)
      {
        case '{':
          updateVars(ADD_NESTING, vars);
          break;
        case '}':
          updateVars(REMOVE_NESTING, vars);
          break;
        case '/':
          if (*(it + 1) == '/')
          {
            updateVars(ADD_EOLN, vars);
            return;
          }
          if (*(it + 1) == '*')
            updateVars(START_C_STYLE, vars);
          break;
      }
    }
  }
}
