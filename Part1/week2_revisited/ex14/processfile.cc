#include "main.ih"

Vars processfile()
{
  Vars vars;
  string line;
  while (getline(cin, line))
  {
    processline(line, vars);
    updateVars(END_LINE, vars);
  }
  return vars;
}
