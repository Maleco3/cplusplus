#include "main.ih"

void iterativePrintBig(ostream &out, string value)
{
    // Point to 3 positions from the end (place of first separator)
    int insertPosition = value.length() - 3;
    while (insertPosition > 0)
    {
        // Print separator and again shift 3 positions back
        value.insert(insertPosition, "'");
        insertPosition -= 3;
    }
     out << value;
     return;
}
