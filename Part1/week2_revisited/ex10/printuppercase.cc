#include "main.ih"

void printUpperCase()
{
    char character;
    while (cin.get(character))
        cout << char(toupper(character));
}
