#include "main.ih"

void printLowerCase()
{
    char character;
    while (cin.get(character))
        cout << char(tolower(character));
}
