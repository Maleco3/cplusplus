#include "main.ih"

Vars arguments(int const argc, char* const* argv)
{
    // Declare the variables struct and add the program name and version
    Vars variables;
    variables.name = argv[0];
    variables.name.erase(0, variables.name.find_last_of('/') + 1);
    variables.version = 1;

    // Define the long options
    struct option longOpts[] =
    {
        {"capitalize", 0, 0, 'c'},
        {"help", 0, 0, 'h'},
        {"lc", 0, 0, 'l'},
        {"lower-case", 0, 0, 'l'},
        {"uc", 0, 0, 'c'},
        {"version", 0, 0, 'v'},
        {0, }
    };
    // Go through all the given options and set their respective variables
    int opt;
    while ((opt = getopt_long(argc, argv, "hvcl", longOpts, 0)) != -1)
    {
        switch(opt)
        {
            // -h overrules everything
            case 'h':
                variables.mode = USAGE;
                break;
            // -v Overrules everything except -h
            case 'v':
                if (variables.mode != USAGE)
                    variables.mode = VERSION;
                break;
            // Capitalize and lowerCase only overrule the default OK stateo
            // Also if both flags appear together, set mode to error and return
            case 'c':
                if (variables.mode == OK)
                    variables.mode = CAPITALIZE;
                if (variables.mode == LOWER_CASE)
                {
                    variables.mode = ERROR;
                    return variables;
                }
                break;
            case 'l':
                if (variables.mode == OK)
                    variables.mode = LOWER_CASE;
                if (variables.mode == CAPITALIZE)
                {
                    variables.mode = ERROR;
                    return variables;
                }
                break;
            default:
                variables.mode = ERROR;
                return variables;
        }
    }
    return variables;
}
