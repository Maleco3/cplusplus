#include "main.ih"

void usage(Vars variables)
{
    version(variables);
    std::cout <<
        "\nUsage: ./" << variables.name << " [options] < file\n"
        "Where:\n"
        "--capitalize (--uc, c); capitalize the letters in 'file'\n"
        "--lower-case (--lc, l); change all letters in 'file' to lower case\n"
        "--help (-h); view usage information\n"
        "--version (-v); view program version\n\n"
        << variables.name << " processes 'file' and writes the result to the "
                       "standard output stream.";
}
