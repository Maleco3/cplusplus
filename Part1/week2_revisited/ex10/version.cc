#include "main.ih"

void version(Vars const variables)
{
    cout << variables.name << " V " << variables.version << '\n';
}
