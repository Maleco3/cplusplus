#include "main.ih"

int main(int argc, char *argv[])
{
    struct Vars variables = arguments(argc, argv);
    return process(variables);
}
