#include "main.ih"

void updateVars(updateOptions option, Vars &vars)
{
  switch (option)
  {
    case ADD_NESTING:
      ++vars.nested;
      vars.maxNested = max(vars.maxNested, vars.nested);
      break;
    case REMOVE_NESTING:
      --vars.nested;
      break;
    case ADD_EOLN:
      ++vars.eolnCount;
      ++vars.commentScore;
      break;
    case START_C_STYLE:
      ++vars.cStyleCount;
      vars.IN_C_COMMENT = true;
      if (vars.lineCount == 0)
        vars.startingComment = true;
      break;
    case END_C_STYLE:
        vars.IN_C_COMMENT = false;
        if (vars.startingComment)
          vars.commentScore += vars.lineCount + 1;
        else
          ++ vars.commentScore;
    case END_LINE:
      ++vars.lineCount;
      size_t linecountFactor = 1;
      if (vars.lineCount > 30)
        linecountFactor *= 2;
      if (vars.nested > 3)
        linecountFactor *= 2;
      vars.lineScore += linecountFactor;
  }
}
