#include "main.ih"

int main(int argc, char **argv)
{
    string line;
    Vars vars;
    while (getline(cin, line))
    {
      processline(line, vars);
      updateVars(END_LINE, vars);
    }
    report(vars);

}
