#include "main.ih"

void recursivePrintBig(ostream &out, long long value)
{
    if (abs(value) < 1000)
    {
        out << value;
        return;
    }
    recursivePrintBig(out, value/1000);
    out << '\'' << setw(3) << setfill('0') << (abs(value) % 1000);
}
