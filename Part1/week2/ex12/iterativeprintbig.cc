#include "main.ih"

void iterativePrintBig(ostream &out, string value)
{
    int insertPosition = value.length() - 3;
    while (insertPosition > 0)
    {
        value.insert(insertPosition, "'");
        insertPosition -= 3;
    }
     out << value;
     return;
}
