#include "main.ih"

int main(int argc, char **argv)
{
    recursivePrintBig(cout, stoll(argv[1]));
    cout << '\n';
    iterativePrintBig(cout, argv[1]);
    cout << '\n';
}
