#include "main.ih"

int main(int argc, char **argv)
{
    cout << (argc == 2 ? fib(stoull(argv[1])) : rawfib(stoull(argv[1])))
         << '\n';
}
