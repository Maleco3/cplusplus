#include "main.ih"

unsigned long long recursiveFib(unsigned long long value, unsigned long long fibval[])
{
    if (value <= 2)
        return 1;
    if (fibval[value] != 0)
        return fibval[value];
    fibval[value] = recursiveFib(value - 1, fibval) + recursiveFib(value - 2, fibval);
    return fibval[value];
}
