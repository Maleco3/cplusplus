#include "main.ih"

void printUpperCase()
{
    string line;
    while (getline(cin, line))
    {
        transform(line.begin(), line.end(), line.begin(), ::toupper);
        cout << line << '\n';
    }
}
