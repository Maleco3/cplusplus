#include "main.ih"

void printLowerCase()
{
    string line;
    while(getline(cin, line))
    {
        transform(line.begin(), line.end(), line.begin(), ::tolower);
        cout << line << '\n';
    }
}
