#include "main.ih"

size_t process(Vars const variables)
{
    // Check for redirection
    if (isatty(STDIN_FILENO))
    {
        cout << "No file redirection\n";
        if (variables.mode == USAGE)
            usage(variables);
        return 1;
    }

    // Check the mode and act accordingly
    switch(variables.mode)
    {
        case CAPITALIZE:
            printUpperCase();
            return 0;
        case LOWER_CASE:
            printLowerCase();
            return 0;
        case VERSION:
            version(variables);
            return 0;
        case USAGE:
            usage(variables);
            return 0;
        case ERROR:
            cout << "Invalid option\n";
    }
    return 1;
}
