#ifndef INCLUDED_UNITS_H
#define INCLUDED_UNITS_H

#include <cmath>

class Units
{
    enum Conversion
    {
        METERS_IN_NM      = 1852,
        SECONDS_IN_HOUR   = 3600,
        FEET_IN_METER     = 3,
        FEET_IN_3_METER   = 10,
        SECONDS_IN_MINUTE = 60,
        METERS_IN_KM      = 1000,
    };

public:
    static double constexpr kts2m_s(double knots)
    {
        return knots * METERS_IN_NM / SECONDS_IN_HOUR;
    }

    static double constexpr ft2m(double feet)
    {
        return feet * FEET_IN_METER / FEET_IN_3_METER;
    }

    static double constexpr ft_min2m_s(int ftmin)
    {
        return ftmin / SECONDS_IN_MINUTE / FEET_IN_METER / FEET_IN_3_METER;
    }

    static int constexpr nm2m(double nm)
    {
        return int (nm * METERS_IN_NM);
    }

    static int constexpr km2m(double km)
    {
        return int (round(km * METERS_IN_KM));
    }
};

#endif
