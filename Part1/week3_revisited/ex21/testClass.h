#ifndef INCLUDED_TESTCLASS_H_
#define	INCLUDED_TESTCLASS_H_

class TestClass
{
  size_t d_importantNumber;

  public:
    TestClass();

    void importantNumber() const;

  private:
    void setImportantNumber(const size_t iNum);
};

#endif
