#include "cpu.ih"

void CPU::dsp()
{
  Operand lhOperand;
  if (!lvalue(lhOperand))
    return;
  int value1 = dereference(lhOperand);
  cout << value1 << '\n';
}
