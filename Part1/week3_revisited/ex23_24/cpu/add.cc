#include "cpu.ih"

void CPU::add()
{
  Operand lhOperand;
  int value1, value2;
  if (!twoOperands(lhOperand, &value1, &value2))
    return;
  cout << "val1: " << value1 << " val2: " << value2 << endl;
  store(lhOperand, value1 + value2);
}
