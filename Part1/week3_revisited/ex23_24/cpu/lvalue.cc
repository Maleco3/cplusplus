#include "cpu.ih"

bool CPU::lvalue (Operand &operand)
{
  rvalue(operand);
  return (operand.operandType == OperandType::REGISTER or operand.operandType == OperandType::MEMORY);
}
