/*
- First get two operands via operands function!
- dereference them both in the value pointers with dereference();
return true if it all succeeds, else false
*/
#include "cpu.ih"

bool CPU::twoOperands (Operand &lhOperand, int *lhv, int *rhv)
{
    Operand rhOperand;
    if (!operands(lhOperand, rhOperand))
      return false;
    *lhv = dereference(lhOperand);
    *rhv = dereference(rhOperand);
    return true;
}
