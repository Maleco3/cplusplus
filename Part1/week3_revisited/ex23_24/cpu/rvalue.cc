#include "cpu.ih"

bool CPU::rvalue (Operand &operand)
{
  // Check if getting operand succeeds and is not Out Of Bounds
  if (!d_tokenizer.token(operand) ||
  (operand.value < 0) ||
  (operand.operandType == OperandType::REGISTER &&
      operand.value >= NREGISTERS) ||
  (operand.operandType == OperandType::MEMORY &&
      operand.value >= RAM::SIZE))
  {
    error();
    return false;
  }
  return true;
}
