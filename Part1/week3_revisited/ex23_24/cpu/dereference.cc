#include "cpu.ih"

int CPU::dereference (Operand &op)
{
  switch (op.operandType) {
    case OperandType::SYNTAX:
      return 0;
    case OperandType::VALUE:
      return op.value;
    case OperandType::REGISTER:
      return d_registers[op.value];
    case OperandType::MEMORY:
      return d_memory->load(op.value);
  }
  return 0;
}
