#include "cpu.ih"

void CPU::neg()
{
  Operand lhOperand;
  if (!lvalue(lhOperand))
    return;
  int value1 = dereference(lhOperand);
  store(lhOperand, value1 * -1);
}
