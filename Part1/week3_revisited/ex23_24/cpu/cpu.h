#ifndef INCLUDED_CPU_
#define INCLUDED_CPU_
#include "../enums/enums.h"
#include "../memory/memory.h"
#include "../tokenizer/tokenizer.h"

enum VARS {
  NREGISTERS = 5
};

class CPU
{
  int d_registers[NREGISTERS];
  Memory *d_memory;
  Tokenizer d_tokenizer;

  public:
    CPU(Memory *externMem);
    void start();

  private:
    bool twoOperands (Operand &lhOperand, int *lhv, int *rhv);
    int dereference (Operand &op);
    bool operands (Operand &lh, Operand &rh);

    bool lvalue(Operand &operand);
    bool rvalue(Operand &operand);

    void store (Operand &operand, int value);

    void error ();
    void mov ();
    void add ();
    void sub ();
    void mul ();
    void div ();
    void neg ();
    void dsp ();
};

#endif
