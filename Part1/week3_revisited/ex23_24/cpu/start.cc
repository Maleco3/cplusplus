#include "cpu.ih"

void CPU::start()
{
  Opcode opcode;
  while (true)
  {
    opcode = d_tokenizer.opcode();
    switch (opcode)
    {
      case Opcode::MOV:
        mov();
        break;
      case Opcode::ADD:
        add();
        break;
      case Opcode::SUB:
        sub();
        break;
      case Opcode::MUL:
        mul();
        break;
      case Opcode::DIV:
        div();
        break;
      case Opcode::NEG:
        neg();
        break;
      case Opcode::DSP:
        dsp();
        break;
      case Opcode::STOP:
        return;
      case Opcode::ERR:
        error();
        break;
    }
    d_tokenizer.reset();
  }
}
