#include "cpu.ih"

CPU::CPU(Memory *externalMem)
:
  d_memory(externalMem)
{
    for (int idx = 0; idx != NREGISTERS; ++idx)
        d_registers[idx] = 0;

}
