#ifndef INCLUDED_MEMORY_
#define INCLUDED_MEMORY_
#include "../enums/enums.h"
#include <cstddef>

class Memory
{
  int d_values[RAM::SIZE];

  public:
    Memory();

    void store(std::size_t address, int value);
    int load(std::size_t address) const;

};

#endif
