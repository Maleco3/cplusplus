#ifndef INCLUDED_TOKENIZER_
#define INCLUDED_TOKENIZER_
#include <string>
#include "../enums/enums.h"

class Tokenizer
{
  public:
    Opcode opcode();
    bool token(Operand &op);
    void reset();

  private:
    int value(OperandType optype, std::string input);
};

#endif
