#include "tokenizer.ih"

void Tokenizer::reset()
{
  cin.clear();
  cin.ignore(numeric_limits<streamsize>::max(), '\n');
}
