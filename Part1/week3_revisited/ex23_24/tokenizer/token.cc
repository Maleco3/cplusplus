#include "tokenizer.ih"

bool Tokenizer::token(Operand &op)
{
  string input;
  cin >> input;
  switch(input[0])
  {
    case '@':
      op.operandType = OperandType::MEMORY;
      op.value = value(OperandType::MEMORY, input.erase(0,1));
      return true;
    case 'a' ... 'z':
      op.operandType = OperandType::REGISTER;
      op.value = value(OperandType::REGISTER, input);
      return true;
    case '0' ... '9':
      op.value = stoi(input);
      op.operandType = OperandType::VALUE;
      return true;
    default:
      op.operandType = OperandType::SYNTAX;
      return false;
  }
}
