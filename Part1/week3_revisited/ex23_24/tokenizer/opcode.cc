/*
converting the first element to an Opcode, which is then returned.
*/
#include "tokenizer.ih"

Opcode Tokenizer::opcode()
{
  string input;
  cin >> input;
  switch (input[2])
  {
    case 'v':
    if (input == "mov")
    {
      return Opcode::MOV;
      break;
    }
    if (input =="div")
    {
      return Opcode::DIV;
      break;
    }
    case 'd':
      if (input =="add")
      {
        return Opcode::ADD;
        break;
      }
    case 'b':
      if (input =="sub")
      {
        return Opcode::SUB;
        break;
      }
    case 'l':
      if (input =="mul")
      {
        return Opcode::MUL;
        break;
      }
    case 'g':
      if (input =="neg")
      {
        return Opcode::NEG;
        break;
      }
    case 'p':
      if (input =="dsp")
      {
        return Opcode::DSP;
        break;
      }
    case 'o':
      if (input =="stop")
      {
        return Opcode::STOP;
        break;
      }
    default:
      return Opcode::ERR;
  }
}
