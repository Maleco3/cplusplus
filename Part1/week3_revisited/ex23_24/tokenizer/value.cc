#include "tokenizer.ih"

int Tokenizer::value(OperandType optype, string input)
{
 switch (optype) {
    case OperandType::VALUE:
    case OperandType::MEMORY:
      return stoi(input);
    case OperandType::REGISTER:
      return input[0] - 'a';
    default: //
      return 0;
    }
}
