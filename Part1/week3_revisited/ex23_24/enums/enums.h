#ifndef INCLUDED_ENUMS_
#define INCLUDED_ENUMS_

enum RAM : int {
  SIZE = 20
};

enum class Opcode
{
  STOP,
  MOV,
  ADD,
  SUB,
  MUL,
  DIV,
  NEG,
  DSP,
  ERR
};

enum class OperandType
{
  SYNTAX,
  VALUE,
  REGISTER,
  MEMORY
};

struct Operand
{
  OperandType operandType;
  int value;
};
#endif
