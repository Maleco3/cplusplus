#include "search.ih"

Search::Search(int argc, char *argv[])
{
  bool caseIgnore = false;
  bool blanks = false;
  bool lineNumbers = true;
  bool lineContents = true;

  int c;
  while ((c = getopt(argc, argv, "wiln")) != -1)
  {
    switch(c)
    {
      case 'i':
        caseIgnore = true;
        break;
      case 'w':
        blanks = true;
        break;
      case 'l':
        lineNumbers = false;
        break;
      case 'n':
        lineContents = false;
        break;
    }
  }
  d_match = (!caseIgnore and !blanks) ? match :
            (caseIgnore and !blanks)  ? matchCaseIgnore :
            (!caseIgnore and blanks)  ? matchBlanks :
                                        matchBlanksCaseIgnore;
}
