#ifndef INCLUDED_SEARCH_
#define INCLUDED_SEARCH_

#include <string>
#include <getopt.h>

class Search
{
  bool &(Search::*d_match)(std::string str1, std::string str2);
  size_t d_findTarget;
  size_t d_showNrs;
  size_t d_showLine;

  public:
    Search(int argc, char *argv[]);
    void run(std::string text);

  private:
    void noOp();
    bool match(std::string str1, std::string str2);
    bool matchCaseIgnore(std::string str1, std::string str2);
    bool matchBlanks(std::string str1, std::string str2);
    bool matchBlanksCaseIgnore(std::string str1, std::string str2);
    void search();
    void showNumbers();
    void showLines();
};

#endif
