#include "main.ih"

void strcpy2(char dst[], char const src[])
{
  for (size_t idx = 0; src[idx]; ++idx)
    dst[idx] = src[idx];
}
