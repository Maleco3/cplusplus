#include "strings.ih"

void Strings::add(string const str)
{
  reserve(d_size + 1);              // Make sure the new string fits
  new(d_str + d_size) string(str);
  ++d_size;
}
