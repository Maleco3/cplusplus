// Called when the capacity must be doubled
#include "strings.ih"

void Strings::reserve()
{
  string *newMemory= rawStrings(d_capacity << 1); // Create new block of memory

  for (size_t idx = 0; idx != d_size; ++idx) // Copy old pointers to new memory
    new(newMemory + idx) string(d_str[idx]);

  destroy();
  d_str = newMemory;
  d_capacity <<= 1;
}
