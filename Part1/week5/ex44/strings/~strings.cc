#include "strings.ih"

Strings::~Strings()
{
  for (size_t idx = 0; idx != d_size; ++idx)
    d_str[idx].~string();
  operator delete(d_str);
}
