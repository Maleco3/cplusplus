#include "strings.ih"

void Strings::reserve(size_t size)
{
  // Enlarge the capacity until size can fit
  while (size >= d_capacity)
    reserve();
}
