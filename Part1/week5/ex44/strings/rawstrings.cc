#include "strings.ih"

string *Strings::rawStrings(size_t size)
{
  return static_cast<string *>(operator new(size * sizeof(string)));
}
