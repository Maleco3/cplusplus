#include "strings.ih"

Strings::Strings(char **environ)
:
  Strings()
{
  size_t count = 0;
  for (char *s = *environ; s; ++count)
    s = *(environ + count);

  reserve(count - 1);

  for (size_t idx = 0; idx != count - 1; ++idx)
    add(environ[idx]);
}
