#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_
#include <iosfwd>


class Strings
{
  size_t d_size;          // Number of pointers in use
  size_t d_capacity;  // Capacity of the class
  std::string *d_str;

  public:
    Strings(int argc, char *argv[]);  //1
    Strings(char **environ);          //2
    Strings();                        //3
    ~Strings();

    std::string       &at(size_t index);
    std::string const &at(size_t index) const;
    size_t size() const;

    void add(std::string const string); // add1
    void add(char const *cstring); // add2
    static void stringSwap(Strings &left, Strings &right);
    void destroy();

    std::size_t capacity() const;
    void reserve(size_t size);  // reserve1.cc
    void reserve();             // reserve2.cc
    void resize(size_t size);
    std::string *rawStrings(size_t size);

  private:
    void copy(std::string *one, std::string *two, size_t size);


};

#endif
