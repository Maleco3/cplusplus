#include "strings.ih"

void Strings::destroy()
{
  for (string *sp = d_str + d_size; sp-- != d_str; )
    sp->~string();
  operator delete(d_str);
}
