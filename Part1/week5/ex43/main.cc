#include "main.ih"

extern char **environ;

int main(int argc, char **argv)
{

  Strings clArguments(argc, argv);
  Strings envContents(environ);

  if (envContents.size() >= 4)
    clArguments.add(envContents.at(4));
  size_t length = clArguments.size();
  for (size_t idx = 0; idx != length; ++idx)
    cout << clArguments.at(idx) << '\n';

  Strings object = Strings(argc, argv);
  Strings const constObject = Strings(argc, argv);
  cout << object.at(0) << '\n'        // Calls the non-const at member function
       << constObject.at(0) << '\n';  // Calls the const at member function

  // Swap example
  cout << "pre-swap:\n"
          "cl(1): " << clArguments.at(1) << "\n"
          "env(1): "<< envContents.at(1) << '\n';
  clArguments.stringSwap(envContents);
  cout << "post-swap:\n"
          "cl(1): " << clArguments.at(1) << "\n"
          "env(1): "<< envContents.at(1) << '\n';
}
