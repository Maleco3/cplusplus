#include "strings.ih"

Strings::Strings(char **environ)
:
  Strings()
{
  size_t count = countVar(environ);
  reserve(count);

  for (size_t idx = 0; idx != count - 1; ++idx)
    add(environ[idx]);
}
