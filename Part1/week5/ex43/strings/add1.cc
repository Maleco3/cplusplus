#include "strings.ih"

void Strings::add(string str)
{
  reserve(d_size + 1);              // Make sure the new string fits
  d_str[d_size] = new string(str);  // Add the new string
  ++d_size;
}
