#include "strings.ih"

void Strings::stringSwap(Strings &other)
{
  // Swap the objects contents by built-in swap functions
  swap(d_size, other.d_size);
  swap(d_capacity, other.d_capacity);
  swap(d_str, other.d_str);
}
