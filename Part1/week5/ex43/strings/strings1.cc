#include "strings.ih"

Strings::Strings(int argc, char *argv[])
:
  Strings()
{
  reserve(argc);

  for (int idx = 0; idx != argc; ++idx)
    add(argv[idx]);
}
