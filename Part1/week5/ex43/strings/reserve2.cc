// Called when the capacity must be doubled
#include "strings.ih"

void Strings::reserve()
{
  string **newMemory= rawPointers(d_capacity << 1); // Create new block of memory

  for (size_t idx = 0; idx != d_size; ++idx) // Copy old pointers to new memory
    newMemory[idx] = d_str[idx];

  destroy();
  d_str = newMemory;
  d_capacity <<= 1;
}
