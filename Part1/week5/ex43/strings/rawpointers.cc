#include "strings.ih"

string **Strings::rawPointers(size_t size)
{
  return new string *[size];
}
