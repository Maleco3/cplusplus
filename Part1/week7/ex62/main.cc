#include "main.ih"

void printMatrix(Matrix test, size_t dim)
{
  for (size_t row = 0; row != dim; ++row)
  {
    double *row1 = test.row(row);
    for (size_t idx = 0; idx != dim; ++idx)
      cout << (int)*(row1 + idx) << ' ';
    cout << '\n';
  }
}

int main(int argc, char **argv)
{
  // size_t dim = 5;
  // Matrix test = Matrix(dim, dim);
  // printMatrix(test, dim);
  // Matrix test2(Matrix::identity(5));
  // // printMatrix(test2, dim);
  // // Matrix test3 = Matrix::identity(8);
  // // printMatrix(test3, 8);
  // printMatrix(Matrix::identity(8), 8);
  Matrix q = Matrix({{10,2,3},{4,5,6},{7,8,9}});
  printMatrix(q, 3);
  q = q.tr();
  printMatrix(q, 3);
  Matrix p = Matrix({{1,2,3},{4,5,6}});
  printMatrix(p, 3);
  printMatrix(p.transpose(), 3);

  Matrix
}
