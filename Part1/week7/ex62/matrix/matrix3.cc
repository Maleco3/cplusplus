#include "matrix.ih"

Matrix::Matrix(Matrix const &other)
:
    d_nRows(other.d_nRows),
    d_nCols(other.d_nCols),
    d_data(rawData(d_nRows * d_nCols))
{
  for (size_t idx = d_nRows * d_nCols; idx--;)
    new (&d_data[idx]) double(other.d_data[idx]);
}
