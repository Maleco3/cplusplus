#include "matrix.ih"

Matrix Matrix::identity(int dim)
{
  Matrix *ident = new Matrix(dim, dim);
  for (size_t diag = 0; diag != dim; ++diag)
  {
    size_t idx = diag * dim + diag;
    cout << "diag idx: " << idx << endl;
    *(ident->d_data + diag * dim + diag) = 1;
  }
  return *ident;
}
