#include "matrix.ih"

Matrix &Matrix::tr()
{
  if (d_nRows != d_nCols)
  {
    cout << "Error: the current matrix is not a square matrix\n";
    exit(1);
  }
  for (size_t row = 0; row != d_nRows; ++row)
  {
    for (size_t col = row + 1; col != d_nCols; ++col)
    {
      size_t idx1 = row * d_nCols + col;
      size_t idx2 = col * d_nCols + row;
      if (idx1 != idx2)
        swap(d_data[idx1],d_data[idx2]);
    }
  }
  cout << d_data[0] << endl;
  return *this;
}
