#include "matrix.ih"

Matrix::Matrix(initializer_list<initializer_list<double>> matrix)
:
  d_nRows(matrix.size()),
  d_nCols(matrix.begin()[0].size()),
  d_data(rawData(d_nRows*d_nCols))
{
  for (size_t row = 0; row != d_nRows; ++row)
  {
    if (matrix.begin()[row].size() != d_nCols)
    {
      cout << "Warning: different sized initializer lists\n";
      exit(1);
    }
    for (size_t col = 0; col != d_nCols; ++col)
    {
      size_t idx = row * d_nCols + col;
      new (&d_data[idx]) double(matrix.begin()[row].begin()[col]);
    }
  }
}
