#include "matrix.ih"

double *Matrix::rawData(size_t nDoubles)
{
  return static_cast<double *>(operator new (nDoubles * sizeof(double)));
}
