#include "matrix.ih"

Matrix Matrix::transpose()
{
  Matrix *transp = new Matrix(d_nCols, d_nRows);

  for (size_t row = 0; row != d_nRows; ++row)
  {
    for (size_t col = 0; col != d_nCols; ++col)
    {
      size_t idx1 = row * d_nCols + col; //idx of the original
      size_t idx2 = col * d_nRows + row; //idx of the transponent
        transp->d_data[idx2] = d_data[idx1];
    }
  }
  cout << d_data[0] << endl;
  return *transp;
}
