#include "matrix.ih"

Matrix &Matrix::operator=(const Matrix &rvalue)
{
  destroy();
  d_nRows = rvalue.d_nRows;
  d_nCols = rvalue.d_nCols;
  d_data = rawData(d_nRows * d_nCols);
  for (size_t idx = d_nRows * d_nCols; idx--;)
    new (&d_data[idx]) double(rvalue.d_data[idx]);
  return *this;
}
