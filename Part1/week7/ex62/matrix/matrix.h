#ifndef INCLUDED_MATRIX_
#define INCLUDED_MATRIX_

#include <initializer_list>
#include <iosfwd>

class Matrix
{
    size_t d_nRows = 0;
    size_t d_nCols = 0;
    double *d_data;

    public:
        Matrix() = default;
        Matrix(size_t rows, size_t cols);
        Matrix(std::initializer_list<std::initializer_list<double>> matrix);
        Matrix(Matrix const &other);  // 3


        ~Matrix();

        Matrix &operator=(Matrix const &rvalue); // assignment

        size_t nRows() const;
        size_t nCols() const;

        double *row(size_t idx);
        double *row(size_t idx) const;
        static Matrix identity(int dim);
        Matrix &tr();
        Matrix transpose();

    private:
      double * rawData(size_t nDoubles);
      void destroy();
};

inline size_t Matrix::nCols() const
{
    return d_nCols;
}

inline size_t Matrix::nRows() const
{
    return d_nRows;
}

inline double *Matrix::row(size_t idx)
{
  return d_data + idx * d_nCols;
}

inline double *Matrix::row(size_t idx) const
{
  return d_data + idx * d_nCols;
}

#endif
