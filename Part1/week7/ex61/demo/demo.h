#ifndef INCLUDED_DEMO_
#define INCLUDED_DEMO_

#include <iostream>

class Demo
{
    public:
        Demo();
        Demo(Demo &other);
        Demo(Demo &&rvalue);

        ~Demo();

        Demo &operator=(Demo &tmp);
        Demo &operator=(Demo &&rvalue);

    private:
};

inline Demo::Demo()
{
    std::cout << "Default constructor\n";
}

inline Demo::Demo(Demo &other)
{
    std::cout << "Copy constructor\n";
}

inline Demo::Demo(Demo &&rvalue)
{
    std::cout << "Move constructor\n";
}

inline Demo::~Demo()
{
    std::cout << "Destructor\n";
}

inline Demo &Demo::operator=(Demo &tmp)
{
    std::cout << "Overloaded assignment operator\n";
    return *this;
}

inline Demo &Demo::operator=(Demo &&rvalue)
{
    std::cout << "Overloaded move assignment operator\n";
    return *this;
}

#endif
