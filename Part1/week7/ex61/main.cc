#include "main.ih"
#include <algorithm>
#include <iostream>

Demo factory()
{
    std::cout << "Factory function\n";
    Demo tmp;
    return tmp;
}

int main(int argc, char **argv)
{
    std::cout << "\tCopy elision: \n";
    Demo test1 = Demo(Demo());          // copy elision
    std::cout << "\n\tMove constructor: \n";
    Demo test2(std::move(factory()));   // Using move constructor
    std::cout << "\n\tCopy assignment: \n";
    test1 = test2;                      // Copy assignment
    std::cout << "\n\tMove assignment: \n";
    test1 = std::move(test2);           // Move assignment
}
