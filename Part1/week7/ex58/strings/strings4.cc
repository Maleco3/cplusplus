#include "strings.ih"

Strings::Strings(Strings const &other)
:
  d_size(other.d_size),
  d_capacity(other.d_capacity),
  d_str(rawStrings(d_capacity))
{
  for(size_t idx = 0; idx != d_size; ++idx)
    new (&d_str[idx]) string(other.d_str[idx]);
}
