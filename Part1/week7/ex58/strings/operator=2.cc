#include "strings.ih"

Strings &Strings::operator=(Strings &&rvalue)
{
  swap(rvalue);
  return *this;
}
