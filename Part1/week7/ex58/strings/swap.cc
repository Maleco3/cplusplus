#include "strings.ih"

void Strings::swap(Strings &tmp)
{
    std::swap(d_size, tmp.d_size);
    std::swap(d_capacity, tmp.d_capacity);
    std::swap(d_str, tmp.d_str);
}
