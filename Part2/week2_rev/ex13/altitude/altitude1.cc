#include "altitude.ih"

Altitude::Altitude(Time const &time, Coordinates &coord, 
                   Speed &speed, Units const &units)
:
    d_time(time),
    d_coord(coord),
    d_speed(speed),
    d_units(units)
{}
