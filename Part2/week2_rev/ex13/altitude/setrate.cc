#include "altitude.ih"

void Altitude::setRate(double rate)     // m_s
{
    bool climbing = d_coord.z() < d_reqAlt;
    char const *direction = climbing ? "climb" : "descend";

    if (rate > Limits::MAX_CLIMBRATE)
    {
        cerr <<
            "Requested rate of " << direction <<
                " (" << d_units.climbRate(rate) << ") exceeds " << 
                    d_units.climbRate(Limits::MAX_CLIMBRATE) << "\n"
            "Rate of " << direction << " reduced to " << 
                    d_units.climbRate(Limits::MAX_CLIMBRATE) << '\n';
        rate = Limits::MAX_CLIMBRATE;
    }
    else
        cerr << direction << "ing at " << d_units.climbRate(rate) << '\n';

    d_rate = rate;
    if (not climbing)
        d_rate = -d_rate;    
}





