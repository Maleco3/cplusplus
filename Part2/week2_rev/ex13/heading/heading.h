#ifndef INCLUDED_HEADING_
#define INCLUDED_HEADING_

class Time;
class Speed;
class Coordinates;
class Altitude;
class Units;

class Heading
{
    enum TurnType
    {
        LEFT,
        RIGHT
    };

    Time const &d_time;
    Speed const &d_speed;
    Coordinates &d_coord;
    Altitude &d_altitude;
    Units const &d_units;

    TurnType d_turnType = LEFT;
    double d_reqHdg = 0;    // radians, requested heading

    public:
        Heading(Time const &time, Speed const &speed, Coordinates &coord,
                Altitude &altitude, Units const &units);

                                        // all headings in radians
        void turnTo(int direction, double radHdg, double acceleration);
};

#endif
