#include "heading.ih"

Heading::Heading(Time const &time, Speed const &speed, Coordinates &coord,
                 Altitude &altitude, Units const &units)
:
    d_time(time),
    d_speed(speed),
    d_coord(coord),
    d_altitude(altitude),
    d_units(units)
{}
