#include "connection.ih"

void Connection::activate(string const &pin)
{
    istringstream in(d_users.tail());   // tail of the database line
    in >> d_nick >> d_balance;

    cout << "Happy hunting, " << d_nick << "!\n";
    d_active = true;
    d_pin = pin;
    d_time = time(0);

    Lock lock(d_fifo);
    ofstream out(d_fifo);
    out << "r " << d_pin << ' ' << d_nick << ' ' << d_balance << '\n';
}
