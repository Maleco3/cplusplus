#include "connection.ih"

bool Connection::login(string const &line) // without the 'r'
{
    if (d_active)
    {
        cout << d_nick << " already logged in. Logout (quit) first\n";
        return true;
    }

    string pin;
    istringstream in(line);
    if (not (in >> pin))
    {
        cout << "r command without pin\n";
        return true;
    }

    switch (d_users.activate(pin))
    {
        case DataBase::UNKNOWN_PIN:
            cout << "Unknown pin: " << pin << '\n';
        break;

        case DataBase::ALREADY_ACTIVE:
            cout << pin << " already active elsewhere\n";
        break;

            default:    // PIN_OK
            activate(pin);
        break;
    }

    return true;
}
