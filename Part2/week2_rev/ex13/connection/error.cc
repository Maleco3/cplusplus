#include "connection.ih"

bool Connection::error(string const &line)
{
    cout << "Command " << s_command[s_size - 1].key << " not implemented\n";
    return true;
}
