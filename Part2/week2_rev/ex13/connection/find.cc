#include "connection.ih"

bool (Connection::*Connection::find(int key))(string const &)
{
    s_command[s_size - 1].key = key;             // set the final search ch

    auto *ret = s_command;
    while (ret->key != key)
        ++ret;

    return ret->ptr;
}
