#include "connection.ih"

Connection::KeyCommand Connection::s_command[] =
{
    { 'r', &Connection::login },
    { '.', &Connection::dot   },
    { 'q', &Connection::quit  },
    { ' ', &Connection::error }
};

size_t Connection::s_size = 
                        sizeof(s_command) / sizeof(Connection::KeyCommand);
