#ifndef INCLUDED_CONNECTION_
#define INCLUDED_CONNECTION_

#include <string>
#include <ctime>

#include "../database/database.h"

class Connection
{
    DataBase d_users;
    bool d_active = false;

    time_t d_time;          // initialized at login
    long long   d_balance;
    std::string d_nick;
    std::string d_pin;
    std::string d_fifo{"/tmp/0"};

    struct KeyCommand
    {
        int key;
        bool (Connection::*ptr)(std::string const &cmd);
    };

    static KeyCommand s_command[];
    static size_t s_size;

    public:
        Connection(char const *users);
        void process();
        
    private:
        bool (Connection::*find(int key))(std::string const &);

        bool login(std::string const &cmd);
        void activate(std::string const &pin);

        bool dot(std::string const &cmd);
        bool quit(std::string const &cmd);
        bool error(std::string const &cmd);
};
        
#endif

