#include "connection.ih"

bool Connection::dot(string const &line)
{
    if (not d_active)
        cout << "Not logged in yet. Login first using 'r'\n";
    else
        cout << "Balance: " << d_balance + time(0) - d_time << '\n';

    return true;
}
