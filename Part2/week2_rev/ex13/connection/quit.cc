#include "connection.ih"

bool Connection::quit(string const &line)
{
    if (not d_active)
        cout << "You're not logged in yet. But quitting anyway\n";
    else
    {
        d_balance += time(0) - d_time;
        d_users.logoff(d_pin, d_balance);

        Lock lock(d_fifo);
        ofstream out(d_fifo);
        out << "q " << d_pin << '\n';

        cout << "Bye " << d_nick << ", final balance: " << d_balance << '\n';
    }
    return false;
}
