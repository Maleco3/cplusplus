#include "connection.ih"

void Connection::process()
{
    string line;
    decltype(&Connection::login) ptr;

    while (true)
    {
        cout << "? ";                       // prompt
        
        if (not getline(cin, line))         // no more input, then quit
            line = "q";

        if (line.length() == 0)             // no input, try again
            continue;

        ptr = find(line[0]);
        line.erase(0, line.find_first_not_of(" \t", 1));

        if (not (this->*ptr)(line))         // execute the command
            return;
    }                                       
}
