#include "speed.ih"

void Speed::set(double speed)
{
    d_reqSpeed = speed;

    if (d_reqSpeed < Limits::TO_SPEED)
    {
        cerr << "Speed " << d_units.speed(speed) << 
                " is too low. Increased to " << 
                d_units.speed(Limits::TO_SPEED) << '\n';

        d_reqSpeed = Limits::TO_SPEED;
    }
    else if (d_reqSpeed > Limits::V_MAX)
    {
        cerr << "Speed " << d_units.speed(speed) << 
                " exceeds Vmax (" << d_units.speed(Limits::V_MAX) << "). "
                "Using " << d_units.speed(Limits::V_MAX) << " instead.\n";
        d_reqSpeed = Limits::V_MAX;
    }
    else
        cerr << "Setting speed to " << d_units.speed(speed) << '\n';
}





