#include "speed.ih"

Speed::Speed(Time const &time, Coordinates &coord, Units const &units)
:
    d_time(time),
    d_coord(coord),
    d_units(units)
{}
