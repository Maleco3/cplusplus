#include "main.ih"

int main(int argc, char **argv)
{
  Matrix mat1({{1,2},{3,4}});
  Matrix *mat2 = factory(mat1, 10);
  Matrix mat3({{5, 6},{7, 8}});
  Matrix *mat4 = factory(mat3, 10);
  cout << *mat4[1][1] << '\n';
}
