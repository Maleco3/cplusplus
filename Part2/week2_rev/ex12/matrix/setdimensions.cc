#include "matrix.ih"

void Matrix::setDimensions(size_t nRows, size_t nCols)
{
    size_t nextSize = nCols * nRows;

    if (nextSize > size())
    {
        double *next = new double[nextSize];
        delete[] d_data;
        d_data = next;
    }

    d_nRows = nRows;
    d_nCols = nCols;
}
    
