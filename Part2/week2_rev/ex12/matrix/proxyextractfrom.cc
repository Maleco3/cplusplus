#include "matrix.ih"

istream &Matrix::Proxy::extractFrom(istream &in)
{
    return d_direction == Matrix::BY_ROWS ? extractRows(in) : extractCols(in);
}

