#include "matrix.ih"

Matrix operator+(Matrix const &lhs, Matrix const &rhs)
{
    Matrix ret(lhs);
    ret.add(rhs);
    return ret;
}
