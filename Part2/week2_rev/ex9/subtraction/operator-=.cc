#include "subtraction.ih"

Operations &Subtraction::operator-=(Operations const &rhs)
{
  static_cast<Operations *>(this)->subBinop(rhs);
  return *static_cast<Operations *>(this);
}
