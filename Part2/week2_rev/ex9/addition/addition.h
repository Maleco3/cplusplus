#ifndef INCLUDED_ADDITION_
#define INCLUDED_ADDITION_

class Operations;

class Addition
{
public:
  Operations &operator+=(Operations const &rhs);
};

Operations operator+(Operations const &lhs, Operations const &rhs);

#endif
