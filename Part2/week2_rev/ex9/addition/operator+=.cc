#include "addition.ih"

Operations &Addition::operator+=(Operations const &rhs)
{
  static_cast<Operations *>(this)->addBinop(rhs); 
  return *static_cast<Operations *>(this);
}
