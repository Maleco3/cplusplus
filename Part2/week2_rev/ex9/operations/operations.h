#ifndef INCLUDED_OPERATIONS_
#define INCLUDED_OPERATIONS_

#include "../binops/binops.h"
#include <iostream>

class Operations: public Binops
{
  friend class Binops;

public:

  private:
    void add(Operations const &rhs);
    void sub(Operations const &rhs);
};

inline void Operations::add(const Operations &rhs)
{
  std::cout << "Addition\n";
}

inline void Operations::sub(const Operations &rhs)
{
  std::cout << "Subtraction\n";
}

#endif
