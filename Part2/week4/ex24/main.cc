// A simple program that counts inputted words in an ordered map and prints them
#include <iostream>
#include <string>
#include <map>

using namespace std;

int main(int argc, char **argv)
{
  std::map<std::string, size_t> mapStrings;
  std::string input;

  while (cin >> input)
    ++mapStrings[input];
  for (auto &string : mapStrings)
    cout << string.first << ": " << string.second << '\n';
}
