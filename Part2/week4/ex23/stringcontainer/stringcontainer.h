#ifndef INCLUDED_STRINGCONTAINER_
#define INCLUDED_STRINGCONTAINER_

#include <vector>
#include <string>
#include <iostream>

class StringContainer
{
  std::vector<std::string> d_vec;

    public:
      StringContainer() = default;
      StringContainer(std::vector<std::string> const &vec);
      void swap(StringContainer &other);
      std::vector<std::string> &vec(); // return a reference to vec
      void add(std::string const &str);

    private:
};

inline StringContainer::StringContainer(std::vector<std::string> const &vec)
:
  d_vec(vec)
{}

inline void StringContainer::swap(StringContainer &other)
{
  d_vec.swap(other.d_vec);
}

inline std::vector<std::string> &StringContainer::vec()
{
  return d_vec;
}

inline void StringContainer::add(std::string const &str)
{
  d_vec.push_back(str);
}
#endif
