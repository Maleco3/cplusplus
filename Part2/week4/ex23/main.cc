// A simple program that reads inputted words in an ordered list and prints them
#include <iostream>
#include <string>
#include <set>
#include <vector>

#include "stringcontainer/stringcontainer.h"

using namespace std;

int main(int argc, char **argv)
{
  std::set<string> setStrings;
  std::vector<string> vectorStrings;
  std::string input;

  while (cin >> input)
    setStrings.insert(input);

  for (auto &input : setStrings)
    vectorStrings.push_back(input);

  cout << "Vector size: " << vectorStrings.size() << '\n'
       << "Vector capacity: " << vectorStrings.capacity() << '\n';

  vectorStrings.push_back("additionalword");

  cout << "Vector size: " << vectorStrings.size() << '\n'
       << "Vector capacity: " << vectorStrings.capacity() << '\n';

  std::vector<string>(vectorStrings).swap(vectorStrings);

  cout << "Vector size: " << vectorStrings.size() << '\n'
       << "Vector capacity: " << vectorStrings.capacity() << '\n';

  StringContainer sc;
  for (auto input : setStrings)
    sc.add(input);
  sc.add("additionalword");

  cout << sc.vec().capacity() << '\n';
  StringContainer sc2(sc.vec());
  sc.swap(sc2);
  cout << sc.vec().capacity() << '\n';
}
