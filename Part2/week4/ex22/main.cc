// A simple program that reads inputted words in an ordered list and prints them
#include <iostream>
#include <string>
#include <set>

using namespace std;

int main(int argc, char **argv)
{
  std::set<string> setStrings;
  std::string input;

  while (cin >> input)            // Read the strings
    setStrings.insert(input);
  for (auto &string : setStrings)  // Print the inputted strings
    cout << string << '\n';
}
