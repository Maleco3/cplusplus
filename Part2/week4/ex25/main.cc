#include <unordered_map>
#include <string>
#include <iostream>

#include <algorithm>
#include <numeric>

using namespace std;

int main(int argc, char **argv)
{
  unordered_multimap<string, string> container;
  // container.insert(std::pair<string, string>("Test", "test"));
  // container.insert(std::pair<string, string>("Test", "test"));
  // container.insert(std::pair<string, string>("Tes", "test"));

  size_t nUniqueKeys = accumulate(container.begin(), container.end(), 0,
                                  [&container](size_t count, unordered_multimap<string, string>::value_type value)
  {
    if(container.count(value.first) == 1)
      ++count;
    return count;
  } );
  cout << "There are " << nUniqueKeys << " in the container\n";
}
