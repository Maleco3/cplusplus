#ifndef INCLUDED_SIGNALDEMO_
#define INCLUDED_SIGNALDEMO_

#include "../signalhandler/signalhandler.h"

class SignalDemo : public SignalHandler
{
    public:
      void run();

    private:
};

#endif
