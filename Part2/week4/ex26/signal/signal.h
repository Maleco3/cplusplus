#ifndef INCLUDED_SIGNAL_
#define INCLUDED_SIGNAL_

<<<<<<< HEAD
#include <cstddef>
#include <unordered_map>
#include "../signalhandler/signalhandler.h"

#include <iostream>

class Signal
{
  // Signal keeps tracking of any given signal handlers
  // The handlers are stored in an unordered multimap,
  // so that multiple keys can be stored, and the order of
  // insertion if followed
  std::unordered_multimap<size_t, SignalHandler> d_signalhandlers;

  static Signal *s_signalInstance; // Points to Singleton Signal

  public:
    static Signal *instance();     // Get access to the singleton
    Signal();                      // Declares the instance pointer
    ~Signal();

    void add(size_t signum, SignalHandler &object);
    void remove(size_t signum, SignalHandler &object);
    void ignore(size_t signum);
    void reset(size_t signum);

  private:
};


=======
#include <iosfwd>
#include "../signalhandler/signalhandler.h"

class Signal
{
    public:
        void add(size_t signum, SignalHandler &object);
        void remove(size_t signum, SignalHandler &object);
        void ignore(size_t signum);
        void reset(size_t signum);
    private:
};

>>>>>>> 5c3f94410a44919d0bff5601a4207c708208e904
#endif
