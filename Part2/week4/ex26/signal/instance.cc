#include "signal.ih"

Signal *Signal::instance()
{
  std::cout << "called instance" << '\n';
  if (!s_signalInstance)
    s_signalInstance = new Signal;
  return s_signalInstance;
}
