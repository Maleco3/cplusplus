#include "main.ih"

typedef packaged_task<double (double*, double*, double*)> Task;

double innerProduct(double *first1, double *last1, double *first2)
{
  return inner_product(first1, last1, first2, 0);
}

void compute(double lhs[][5], double rhsT[][5], future<double> fut[][6])
{
  for (size_t row = 0; row != 4; ++row)
    for (size_t col = 0; col != 6; ++col)
    {
      Task task(innerProduct);
      fut[row][col] = task.get_future();
      thread(move(task), begin(lhs[row]), end(lhs[row]), begin(rhsT[col])).detach();
    }
}

int main(int argc, char **argv)
{
  double lhs[4][5];
  double rhsT[6][5];
  future<double> fut[4][6];

  for (size_t col = 0; col != 5; ++col)
  {
    for (size_t row = 0; row != 4; ++row)
      lhs[row][col] = row * col;
    for (size_t row2 = 0; row2 != 6; ++row2)
      rhsT[row2][col] = row2 * col;
  }

  compute(lhs, rhsT, fut);

  for (size_t row = 0; row != 4; ++row)
  {
    for (size_t col = 0; col != 6; ++col)
      cout << fut[row][col].get() << ' ';
    cout << '\n';
  }
}
