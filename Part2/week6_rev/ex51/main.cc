#include "main.ih"

typedef packaged_task<double ()> Task;

array<Task, 24> tasks;

mutex jobMutex;
size_t jobsReady = 0;
mutex workersMutex;
size_t workersReady = 0;

mutex mReady;
condition_variable cv;

double innerProduct(double *first1, double *last1, double *first2)
{
  return inner_product(first1, last1, first2, 0);
}

void compute(double lhs[][5], double rhsT[][5], future<double> fut[][6])
{
  for (size_t row = 0; row != 4; ++row)
    for (size_t col = 0; col != 6; ++col)
    {
      Task task(bind(innerProduct, begin(lhs[row]), end(lhs[row]), begin(rhsT[col])));
      fut[row][col] = task.get_future();
      tasks[row*6 + col] = (move(task));
    }
}

void worker()
{
  while(true)
  {
    // Get a new job
    jobMutex.lock();
    size_t job = jobsReady++;
    jobMutex.unlock();

    // Do the job or return
    if (job <= 23)
      tasks[job]();
    else
    {
      // No more jobs possible{
      workersMutex.lock();
      if (++workersReady == 7)
        cv.notify_all();
      workersMutex.unlock();
      return;
    }
  }
}

int main(int argc, char **argv)
{
  double lhs[4][5];
  double rhsT[6][5];
  future<double> fut[4][6];

  // Fill the matrices
  for (size_t col = 0; col != 5; ++col)
  {
    for (size_t row = 0; row != 4; ++row)
      lhs[row][col] = 5;
    for (size_t row2 = 0; row2 != 6; ++row2)
      rhsT[row2][col] = 5;
  }

  compute(lhs, rhsT, fut);        // Create the tasks

  for (size_t idx = 8; idx--; )   // Start the workers
    thread(worker).detach();

  unique_lock<mutex> lck(mReady); // Wait for the workers to finish
  cv.wait(lck);
  for (size_t row = 0; row != 4; ++row)
  {
    for (size_t col = 0; col != 6; ++col)
      cout << fut[row][col].get() << ' ';
    cout << '\n';
  }
}
