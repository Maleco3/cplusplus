#include "main.ih"

void run(promise<double> prom, double *first1, double *last1, double*first2)
{
  prom.set_value(inner_product(first1, last1, first2, 0));
}

void compute(double lhs[][5], double rhsT[][5], future<double> fut[][6])
{
  for (size_t row = 0; row != 4; ++row)
    for (size_t col = 0; col != 6; ++col)
    {
      promise<double> prom;
      fut[row][col] = prom.get_future();
      thread(run, move(prom), begin(lhs[row]), end(lhs[row]), begin(rhsT[col])).detach();
    }
}

int main(int argc, char **argv)
{
  double lhs[4][5];
  double rhsT[6][5];
  future<double> fut[4][6];

  for (size_t col = 0; col != 5; ++col)
  {
    for (size_t row = 0; row != 4; ++row)
      lhs[row][col] = 5;
    for (size_t row2 = 0; row2 != 6; ++row2)
      rhsT[row2][col] = 5;
  }

  compute(lhs, rhsT, fut);

  for (size_t row = 0; row != 4; ++row)
  {
    for (size_t col = 0; col != 6; ++col)
      cout << fut[row][col].get() << ' ';
    cout << '\n';
  }
}
