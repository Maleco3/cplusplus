#include "taskthreads.ih"

void TaskThreads::countSeq()
{
  for (size_t idx = 0; idx != NUMBER_TASKS; ++idx)
  {
    d_threads[idx] = thread(ref(d_tasks[idx]));
    d_threads[idx].join();
  }
}
