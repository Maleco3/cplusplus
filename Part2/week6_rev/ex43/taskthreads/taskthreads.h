#ifndef INCLUDED_TASKTHREADS_
#define INCLUDED_TASKTHREADS_

#include <thread>
#include "../task/task.h"

int isPunct(int ch);
int isVowel(int ch);

class TaskThreads
{
  enum
  {
    VOWELS = 0,
    DIGITS = 1,
    HEXA = 2,
    PUNCT = 3,
    NUMBER_TASKS = 4
  };

  bool d_sequentially = false;
  size_t d_vowels = 0;
  size_t d_digits = 0;
  size_t d_hexadecimals = 0;
  size_t d_punctuations = 0;

  Task d_tasks[NUMBER_TASKS] =
  {
    Task(isVowel, "vowels"),
    Task(isdigit, "digits"),
    Task(isxdigit, "hexadecimals"),
    Task(isPunct, "punctuation")
  };

  std::thread d_threads[NUMBER_TASKS];

  public:
    TaskThreads(int argc, char **argv);

    void run();
    void countPar();
    void countSeq();
    void printOutput();
  private:
};

#endif
