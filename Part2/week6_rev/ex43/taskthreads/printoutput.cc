#include "taskthreads.ih"

void TaskThreads::printOutput()
{
  cout << "vowels: " << d_vowels << '\n'
       << "digits: " << d_digits << '\n'
       << "hexadecimals: " << d_hexadecimals << '\n'
       << "punctuations: " << d_punctuations << '\n';
}
