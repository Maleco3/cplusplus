#include "taskthreads.ih"

void TaskThreads::countPar()
{
  for (size_t idx = 0; idx != NUMBER_TASKS; ++idx)
    d_threads[idx] = thread(ref(d_tasks[idx]));

  for (size_t idx = 0; idx != NUMBER_TASKS; ++idx)
    d_threads[idx].join();
}
