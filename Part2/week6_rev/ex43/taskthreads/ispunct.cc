#include "taskthreads.ih"

int isPunct(int ch)
{
    return (!isalnum(ch) and ch != ' ') ? 1 : 0;
}
