#include <string>

bool isVowel(int ch)
{
  std::string vowels = "aeiouAEIUO";
  return vowels.find(ch) != std::string::npos;
}
