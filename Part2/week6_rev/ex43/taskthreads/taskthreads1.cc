#include "taskthreads.ih"

TaskThreads::TaskThreads(int argc, char **argv)
{
  for (auto &task : d_tasks)
    task.setFilename(argv[1]);

  if (argc > 2)
    d_sequentially = true;
}
