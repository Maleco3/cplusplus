#include "task.ih"

Task::Task(int (*fn)(int), char const *taskLabel)
:
  d_task(fn),
  d_taskLabel(taskLabel)
{}
