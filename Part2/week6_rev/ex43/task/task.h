#ifndef INCLUDED_TASK_
#define INCLUDED_TASK_

#include <string>

class Task
{
  int (*d_task)(int);
  char const *d_taskLabel;
  size_t d_count = 0;
  std::string d_filename;

  public:
    Task(int (*fn)(int), char const *taskLabel);

    size_t count();
    void operator()();
    void setFilename(std::string const &name);

  private:
};

inline void Task::setFilename(std::string const &name)
{
  d_filename = name;
}

inline size_t Task::count()
{
  return d_count;
}

#endif
