#include "safequeue.ih"

void SafeQueue::pop()
{
    unique_lock<mutex> lk(d_mutex);
    while (d_queue.size() == 0)
        d_condition.wait(lk);
    d_queue.pop();
}
