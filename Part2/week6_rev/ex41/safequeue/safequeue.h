#ifndef INCLUDED_SAFEQUEUE_
#define INCLUDED_SAFEQUEUE_

#include <queue>
#include <mutex>
#include <condition_variable>

class SafeQueue
{
  std::queue<int> d_queue;

  mutable std::mutex d_mutex;
  std::condition_variable d_condition;

  public:
    class Proxy;
    Proxy front();
    Proxy back();

    void push(int val);
    void pop();
    bool empty();
    size_t size();
};

class SafeQueue::Proxy
{
  friend Proxy SafeQueue::front();
  friend Proxy SafeQueue::back();

  int d_val;
  SafeQueue *d_parent;
  std::unique_lock<std::mutex> d_ul;

  Proxy(int &val, std::mutex &mut, SafeQueue *sq);

  public:
    ~Proxy();

    int &operator=(int const &rhs);
    operator int const &() const;
};

inline SafeQueue::Proxy::Proxy(int &val, std::mutex &mut, SafeQueue *sq)
:
  d_val(val),
  d_parent(sq),
  d_ul(std::unique_lock<std::mutex>(mut))
{}

inline SafeQueue::Proxy::~Proxy()
{
  d_parent->pop();
}

inline int &SafeQueue::Proxy::operator=(int const &rhs)
{
  return d_val = rhs;
}

inline SafeQueue::Proxy::operator int const &() const
{
  return d_val;
}

#endif
