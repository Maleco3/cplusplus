#ifndef INCLUDED_PIPE_
#define INCLUDED_PIPE_

#include <unistd.h>

class Pipe
{
  enum RW { READ, WRITE };
  int d_pipefd[2];

  public:
    Pipe();

    void readFrom(int fd);
    int readOnly();

    int writeOnly();
    void writtenBy(int fileDescriptor);
    
    void redirect(int d_fd, int alternateFd);

  private:
};

#endif
