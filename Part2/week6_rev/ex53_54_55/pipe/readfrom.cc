#include "pipe.ih"

void Pipe::readFrom(int fd)
{
  readOnly();
  redirect(d_pipefd[READ], fd);
  close(d_pipefd[READ]);
}
