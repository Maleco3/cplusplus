#include "pipe.ih"

void Pipe::writtenBy(int fileDescriptor)
{
  writeOnly();
  redirect(d_pipefd[WRITE], fileDescriptor);
  close(d_pipefd[WRITE]);
}
