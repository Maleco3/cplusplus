#include "cerrextractor.ih"

void CerrExtractor::execute(string const &cmd)
{
  d_pid = fork();

  if (d_pid < 0)
    throw "Forking failed";

  if (d_pid == 0)
  {
    childRedirections();
    childProcess(cmd);
  } else
  {
    parentRedirections();
    parentProcess();
  }
}
