#include "cerrextractor.ih"

void CerrExtractor::childProcess(string const &cmd)
{
  size_t words = count(cmd.begin(), cmd.end(), ' ');

  string word;
  istringstream iss(cmd);
  iss >> word;
  char *mainCommand = new char[word.size()];
  copy(word.begin(), word.end(), mainCommand);

  char **commands = new char *[words];
  size_t idx = 0;
  while (iss >> word)
  {
    commands[idx] = new char[word.size()];
    copy(word.begin(), word.end(), commands[idx]);
    ++idx;
  }
  execv(mainCommand, commands);
}
