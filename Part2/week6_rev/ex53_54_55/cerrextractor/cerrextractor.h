#ifndef INCLUDED_CERREXTRACTOR_
#define INCLUDED_CERREXTRACTOR_

#include <istream>
#include <sys/types.h>
#include "../pipe/pipe.h"

class CerrExtractor : public std::istream
{
  int d_exitcode = -1;
  size_t d_bufSize;
  Pipe d_pipe;
  pid_t d_pid;

  public:
    CerrExtractor(size_t bufSize = 100);

    void execute(std::string const &cmd);
    int ret() const;

  private:
    void childRedirections();
    void parentRedirections();
    void childProcess(std::string const &cmd);
    void parentProcess();

    int waitForChild();
};

#endif
