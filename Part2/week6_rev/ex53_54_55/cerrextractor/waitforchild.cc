#include "cerrextractor.ih"

int CerrExtractor::waitForChild()
{
  int status;
  waitpid(d_pid, &status, 0);
  return WEXITSTATUS(status);
}
