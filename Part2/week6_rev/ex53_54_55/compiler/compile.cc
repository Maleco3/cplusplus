#include "compiler.ih"

bool Compiler::compile()
{
  workForce();
  d_ready.wait();
  return d_activeWorkers = 0;
}
