#ifndef INCLUDED_COMPILER_
#define INCLUDED_COMPILER_

#include <mutex>
#include <condition_variable>
#include <queue>

class Compiler
{
  enum
  {
    N_WORKERS = 8
};
  size_t d_activeWorkers;
  mutable std::mutex d_srcMutex;
  std::condition_variable d_ready;
  std::queue<std::string> d_sources;

  public:
    Compiler();

    bool compile();
  private:
    void workForce();
    void worker();

    void compileOne();
    std::string nextSource();
};

#endif
