#include "compiler.ih"

void Compiler::workForce()
{
  for (size_t count = 0; count != N_WORKERS; ++count)
    thread(worker, ref(*this)).detach();
}
