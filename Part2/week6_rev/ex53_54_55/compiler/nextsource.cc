#include "compiler.ih"

string Compiler::nextSource()
{
  string ret;

  lock_guard<mutex> lk(d_srcMutex);
  if (not d_sources.empty())
  {
    ret = d_sources.back();
    d_sources.pop_back();
  }
  return ret;
}
