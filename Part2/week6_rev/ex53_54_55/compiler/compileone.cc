#include "compiler.ih"

void Compiler::compileOne()
{
  while (true)
  {
    string source = nextSource();
    if (source.empty())
    {
      maybeReady();
      return;
    }
    if (not compile(source))
      return;
  }
}
