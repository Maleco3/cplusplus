#include "safequeue.ih"

void SafeQueue::push(int val)
{
    unique_lock<mutex> lk(d_mutex);
    d_queue.push(val);
    d_condition.notify_all();
}
