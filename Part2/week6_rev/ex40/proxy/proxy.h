#ifndef INCLUDED_PROXY_
#define INCLUDED_PROXY_


class Proxy
{
  friend Proxy SafeQueue::front();
  friend Proxy SafeQueue::back();

  int d_val;
  Proxy(int val, std::mutex &mut);

  public:
    int &operator=(int const &rhs);
    operator int const &() const;
};

inline SafeQueue::Proxy::Proxy(int val, std::mutex &mut)
  :
  d_val(val)
{
  std::unique_lock<std::mutex> lk(mut);
}

inline int &SafeQueue::Proxy::operator=(int const &rhs)
{
  return d_val = rhs;
}

inline SafeQueue::Proxy::operator int const &() const
{
  return d_val;
}

#endif
