#include "ifdstreambuf.ih"

streamsize IFdStreambuf::xsgetn(char *s, streamsize n)
{
  for (streamsize idx = 0; idx !=  n; ++idx, ++s)
  {
    if (idx < bufsize) // Can read from buffer
      s = &d_buffer[idx];
    if (::read(d_FD, s, 1) != 1) // Read from FD
      return -1;
  }
  resetBuffer(n);
  return 0;
}
