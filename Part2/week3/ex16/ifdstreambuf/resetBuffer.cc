#include "ifdstreambuf.ih"

void IFdStreambuf::resetBuffer(streamsize n)
{
  // Reset the buffer with an offset of n
  // If n > bufsize, refresh entire buffer
  // Else shift buffer with n, and read bufsize - n New char
  bool readNew = (n >= bufsize);
  for (streamsize idx = 0; idx != bufsize; ++idx)
  {
    if (readNew)
      ::read(d_FD, &d_buffer[idx], 1);
    else
      d_buffer[idx] = d_buffer[n + idx];
    readNew = (n + idx >= bufsize);
  }
}
