#include "ifdstreambuf.ih"

IFdStreambuf::IFdStreambuf(int FD, Mode mode)
:
    d_mode(mode),
    d_FD(FD)
{
  resetBuffer(bufsize);
}
