#ifndef IFDSTREAMBUF_
#define IFDSTREAMBUF_

#include <ios>
#include <streambuf>
#include <fstream>
#include <istream>

enum Mode
{
    CLOSE_FD,
    KEEP_FD
};

class IFdStreambuf : public std::streambuf
{
    enum { bufsize = 100 };
    Mode d_mode;
    int d_FD;
    char d_buffer[bufsize];

    public:
        explicit IFdStreambuf(Mode mode = KEEP_FD);
        explicit IFdStreambuf(int FD, Mode mode = KEEP_FD);
        ~IFdStreambuf();

        void open(int FD, Mode mode = KEEP_FD);
        void close();

        friend std::istream &operator>>(std::istream &input, IFdStreambuf &ifd)
        {
          input >> ifd.d_buffer[0];
          ifd.resetBuffer(1);
          return input;
        }

    private:
      std::streamsize xsgetn(char *s, std::streamsize n) override;
      void resetBuffer(std::streamsize n);
};

#endif
