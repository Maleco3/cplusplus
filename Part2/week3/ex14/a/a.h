#ifndef INCLUDED_A_
#define INCLUDED_A_

#include "../base/base.h"

class A : public Base
{
    public:
        A()
        :
            Base("A")
        {}

};

#endif
