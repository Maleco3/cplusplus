#ifndef INCLUDED_C_
#define INCLUDED_C_

#include "../base/base.h"

class C : public Base
{
    public:
        C()
        :
            Base("C")
        {}
};

#endif
