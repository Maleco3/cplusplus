#ifndef INCLUDED_BASE_
#define INCLUDED_BASE_

#include <string>
#include <iostream>

class Base
{
    std::string d_type;

    public:
        Base(std::string type)
        :
            d_type(type)
        {}

        Base *clone() const
        {
            std::cout << "clone from " << d_type << '\n';
            return new Base(d_type);
        }

    private:
};

#endif
