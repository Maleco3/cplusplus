#ifndef INCLUDED_BISTREAM_
#define INCLUDED_BISTREAM_

#include <ostream>
#include "../bistreambuffer/bistreambuffer.h"

class BiStream : public std::ostream
{
    BiStreamBuffer d_bibuf;
    public:
        BiStream(std::ostream const &one, std::ostream const &two)
        :
            d_bibuf(one.rdbuf(), two.rdbuf())
        {
            init(&d_bibuf);
        }
};

#endif
