#include "main.ih"

#include <fstream>
#include "bistream/bistream.h"

int main(int argc, char **argv)
{
    ofstream one("one");
    ofstream two("two");

    BiStream ms(std::cout, std::cout);
    // BiStream ms(one, two);

    ms << "Hello " << "world" << endl;
}
