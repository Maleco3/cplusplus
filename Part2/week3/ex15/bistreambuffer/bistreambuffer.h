#ifndef INCLUDED_BISTREAMBUFFER_
#define INCLUDED_BISTREAMBUFFER_

#include <streambuf>
#include <fstream>

class BiStreamBuffer : public std::streambuf
{
    std::streambuf *d_fbuf1;
    std::streambuf *d_fbuf2;

    public:
        BiStreamBuffer(std::streambuf *one, std::streambuf *two)
        :
            d_fbuf1(one),
            d_fbuf2(two)
        {}

    int overflow (int ch)
    // Called for every single character written to streambuf

    {
        d_fbuf1->sputc(ch);
        d_fbuf2->sputc(ch);
        return 0;
    }
};

#endif
