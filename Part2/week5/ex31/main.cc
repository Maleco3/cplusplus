#include <iostream>
#include <string>
#include <vector>
#include <iterator>

using namespace std;

class Line : public string
{
    public:
        friend std::istream &operator>>(std::istream &istr, Line &str)
        {
            return std::getline(istr, str);
        }
};

int main(int argc, char **argv)
{
    vector<string> lines;
    copy(istream_iterator<Line>(cin), istream_iterator<Line>(), back_inserter(lines));
}
