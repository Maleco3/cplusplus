#include <algorithm>
#include <vector>
#include <iostream>

using namespace std;

int main(int argc, char **argv)
{
    vector<size_t> test{1, 2, 3, 4, 5, 1, 2, 3, 1, 2, 3, 3};
    for (size_t st : test)
        cout << st << endl;
    // for_each(test.begin(), test.end(),
    // [](size_t &st){st *= st;});
    test.erase(remove(test.begin(), test.end(), 3), test.end());
    for (size_t st : test)
        cout << st << endl;

}
