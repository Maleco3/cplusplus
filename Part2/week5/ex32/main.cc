#include "algorithm"
#include "iostream"
#include "iterator"

using namespace std;

int main(int argc, char **argv)
{
    std::sort(argv + 1, argv + argc, less_equal<string>{});
    copy(argv + 1, argv + argc, ostream_iterator<string>(cout, "\n"));
    std::sort(argv + 1, argv + argc, greater_equal<string>{});
    copy(argv + 1, argv + argc, ostream_iterator<string>(cout, "\n"));
}
