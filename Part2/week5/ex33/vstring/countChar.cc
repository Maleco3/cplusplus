#include "vstring.ih"

size_t Vstring::countChar(std::string const &str, CharMap &cmap, bool (*accept)(char, CharMap &))
{
    size_t count = 0;
    for_each(str.begin(), str.end(),
    [&](char c) // count, cmap and accept should be references
    {
      if (accept(c, cmap))
        ++count;
    });
    return count;
}
