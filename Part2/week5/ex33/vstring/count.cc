#include "vstring.ih"

size_t Vstring::count(CharMap &cmap, bool (*accept)(char, CharMap &))
{
    size_t count = 0;
    for_each(this->begin(), this->end(),
    [&](string &word) // Count, cmap, and accept should all be references
    {
        count += countChar(word, cmap, accept);
    });
    return count;
}
