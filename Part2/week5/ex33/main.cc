#include "main.ih"

bool vowels(char c, Vstring::CharMap &cmap)
{
    if (string("aeiuoAEIOU").find(c) != string::npos)
    {
        ++cmap[c];
        return true;
    }
    return false;
}

int main(int argc, char **argv)
{
    Vstring vstring(cin);
    Vstring::CharMap vmap;
    cout << "Vowels: " << vstring.count(vmap, vowels) << '\n';

    for_each(vmap.begin(), vmap.end(),
    [](Vstring::CharMap::value_type value)
    {
        cout << value.first << ": " << value.second << '\n';
    });
}
