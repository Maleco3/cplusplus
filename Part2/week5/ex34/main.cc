#include <vector>
#include <algorithm>
#include <iostream>
#include <iterator>
#include <fstream>
#include <string>

using namespace std;

int main(int argc, char **argv)
{
    vector<string> vec1, vec2;
    ifstream file(argv[1]);
    string word;

    copy(istream_iterator<string>(file),
         istream_iterator<string>(),
         back_inserter(vec1));
    file.close();
    file.clear();
    file.open(argv[2]);
    copy(istream_iterator<string>(file),
         istream_iterator<string>(),
         back_inserter(vec2));

    if (find(vec1.begin(), vec1.end(), "extra") != vec1.end() )
    {
        // Sort all occurences of extra to the end of the vector with remove,
        // and erase all these values
        vec1.erase(remove(vec1.begin(), vec1.end(), "extra"), vec1.end());
        // Add all wors from vec2 to vec1, if they are not yet occuring in vec1
        for (string word : vec2)
            if (find(vec1.begin(), vec1.end(), word) == vec1.end())
                vec1.push_back(word);
    }

    // Sort the vec1 vector for the unique algorithm
    sort(vec1.begin(), vec1.end());
    // Create a tmp vector with the unique values of vec1, and swap it with vec1
    vector<string>(vec1.begin(), unique(vec1.begin(), vec1.end())).swap(vec1);
    for (string word : vec1) // Print all words of vec1, seperated by a newline
        cout << word << '\n';
}
