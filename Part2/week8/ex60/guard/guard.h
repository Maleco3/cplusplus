#ifndef INCLUDED_GUARD_
#define INCLUDED_GUARD_

#include <mutex>

class Guard
{
  std::mutex d_mutex;

  public:
    template<typename Type>
    void operator()(Type fun);

  private:
};

template<typename Type>
inline void Guard::operator()(Type fun)
{
  std::lock_guard<std::mutex> lg(d_mutex);
  fun();
}

#endif
