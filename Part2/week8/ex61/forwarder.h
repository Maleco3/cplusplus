#include <utility>

template<typename Function, typename ...Params>
void forwarder(Function function, Params &&...params)
{
  function(std::forward<Params>(params)...);
}
