#include "main.ih"

struct Demo
{
  int x = 1;
};

void fun(int first, int second)
{
  cout << "fun(" << first << ", " << second << ')' << '\n';
}

void fun2(Demo &&dem1, Demo &&dem2)
{
  cout << "fun2: " << dem1.x << ", " << dem2.x << '\n';
}

void incrementer(int &one, int &two, int &three)
{
  ++one;
  ++two;
  ++three;
};

int main()
{
    forwarder(fun, 1, 3);
    forwarder(fun2, Demo{}, Demo{});
    forwarder(plus<string>(), "hello ", "world");

    vector<int> first = {0, 0};
    forwarder(
        [](vector<int> &first, vector<int> const &second,
            vector<int> const &third)
            {
              first[0] += second[0];
              first[1] += third[1];
              cout << "lambda: " << first[0] << ", " << first[1] << '\n';
            },
        first, vector<int>{1, 1}, vector<int>{2, 2});

    int x = 0;
    forwarder(incrementer, x, x, x);
    cout << "incrementer result: " << x << '\n';
}
