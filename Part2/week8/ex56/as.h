template <typename Ret>
Ret as(auto const &x)
{
  return static_cast<Ret>(x);
}
