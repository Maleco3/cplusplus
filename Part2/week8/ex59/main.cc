#include "main.ih"

int main()
{
  nextRange.push(Pair {ia, ia + iaSize});

  thread t1(tsort);
  thread t2(tsort);
  thread t3(tsort);


  t1.join();
  t2.join();
  t3.join();

  for (int val: ia)
    cout << val << ' ';
  cout << '\n';
}
