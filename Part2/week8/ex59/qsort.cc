#include "main.ih"

void Qsort(int *beg, int *end, mutex &mut)
{
  if (end - beg <= 1)
    return;

  int lhs = *beg;
  int *mid = partition(beg + 1, end,
  [&](int arg)
    {
      return arg < lhs;
    }
  );

  swap(*beg, *(mid - 1));

  auto fut1 = async(std::launch::async, Qsort, beg, mid, ref(mut));
  auto fut2 = async(std::launch::async, Qsort, mid, end, ref(mut));
  fut1.get();
  fut2.get();
}
