#include "main.ih"

template<typename Type>
inline void guard(mutex &mut, Type fun)
{
  lock_guard<mutex> lg(mut);
  fun();
}
