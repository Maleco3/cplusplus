#include <iostream>
#include "add.h"
#include "pointerunion.h"

void call1()
{
  PointerUnion pu = { add };
  std::cout << pu.vp << '\n';
}
