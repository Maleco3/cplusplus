#include "matrix.ih"

Matrix &Matrix::operator+=(Matrix const &rhs) &
{
    add(rhs);
    return *this;
}
