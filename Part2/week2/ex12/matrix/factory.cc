#include "matrix.ih"

Matrix *factory(Matrix const &mat, size_t count)
{
    static Matrix mat2;

    struct Fac: public Matrix
    {
        Fac()
        :
            Matrix(mat2)
        {}
    };

    mat2 = mat;
    Matrix *matrices = new Fac[count];
    
    return matrices;
}
