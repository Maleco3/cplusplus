#include "matrix.ih"

Matrix operator+(Matrix &&lhs, Matrix const &rhs)
{
    Matrix ret(move(lhs));
    ret.add(rhs);
    return ret;
}
