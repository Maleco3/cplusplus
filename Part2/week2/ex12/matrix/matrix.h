#ifndef INCLUDED_MATRIX_
#define INCLUDED_MATRIX_

#include <iosfwd>
#include <initializer_list>

class Matrix
{
    size_t d_nRows = 0;
    size_t d_nCols = 0;
    double *d_data = 0;                     // in fact R x C matrix

    // exercise 69 - proxy begin
    class Proxy
    {
        friend class Matrix;
        friend std::istream &operator>>(std::istream &in, Proxy &&prox);

        Matrix &d_mat;

        int d_direction = Matrix::BY_ROWS;
        size_t d_from = 0;
        size_t d_count = ~0UL;
        size_t d_nRows;
        size_t d_nCols;

        Proxy(Matrix &mat, int extractionType, size_t from,
                size_t count, size_t nRows, size_t nCols);

        std::istream &extractFrom(std::istream &in);
        std::istream &extractRows(std::istream &in);
        std::istream &extractCols(std::istream &in);
    };

    friend class Proxy;     // Proxy may access Matrix's members, but can
                            // only be used by Matrix

                            // extraction only uses temporary Proxies
    friend std::istream &operator>>(std::istream &in, Proxy &&mat); // 2
    // exercise 69 - proxy end


    public:
        // exercise 69
        enum Extraction
        {
            BY_ROWS,
            BY_COLS
        };

        typedef std::initializer_list<std::initializer_list<double>> IniList;

        Matrix() = default;
        Matrix(size_t nRows, size_t nCols);         // 1
        Matrix(Matrix const &other);                // 2
        Matrix(Matrix &&tmp);                       // 3
        Matrix(IniList inilist);                    // 4

        ~Matrix();

        Matrix &operator=(Matrix const &rhs);
        Matrix &operator=(Matrix &&tmp);


        size_t nRows() const;
        size_t nCols() const;
        size_t size() const;            // nRows * nCols

        static Matrix identity(size_t dim);

        Matrix &tr();                   // transpose (must be square)
        Matrix transpose() const;       // any dim.

        void swap(Matrix &other);

        // exercise 67
            // removed (as they were only used as stand-ins for operator[]):
            //  double *row(size_t idx);
            //  double const *row(size_t idx) const;
        double *operator[](size_t idx);
        double const *operator[](size_t idx) const;

        // exercise 68
            Matrix &operator+=(Matrix const &rhs)   &;
            Matrix &&operator+=(Matrix const &rhs) &&;

        // exercise 69
            // function call operators returning Proxies to be used with
            // extractions:
        Proxy operator()(size_t nRows, size_t nCols,            // 1
                         Extraction type = BY_ROWS);
        Proxy operator()(Extraction type, size_t from = 0,      // 2
                         size_t count = ~0UL);

    private:
        // exercise 68
        friend Matrix operator+(Matrix const &lhs, Matrix const &rhs); // 1
        friend Matrix operator+(Matrix &&lhs, Matrix const &rhs);      // 2
        void add(Matrix const &rhs);

        // exercise 69
                                                    // called from op() # 2
        size_t extractionLimits(size_t from, size_t count, size_t available);
        void setDimensions(size_t nRows, size_t nCols);

        // exercise 70
        friend bool operator==(Matrix const &lhs, Matrix const &rhs);

        double &el(size_t row, size_t col) const;
};

Matrix *factory(Matrix const &mat, size_t count);

// exercise 69
std::ostream &operator<<(std::ostream &out, Matrix const &mat);
std::istream &operator>>(std::istream &in,  Matrix &mat);               // 1

// exercise 70
inline bool operator!=(Matrix const &lhs, Matrix const &rhs)
{
    return not (lhs == rhs);
}

inline double *Matrix::operator[](size_t idx)
{
    return &el(idx, 0);
}

inline double const *Matrix::operator[](size_t idx) const
{
    return &el(idx, 0);
}

inline size_t Matrix::nCols() const
{
    return d_nCols;
}

inline size_t Matrix::nRows() const
{
    return d_nRows;
}

inline size_t Matrix::size() const
{
    return d_nRows * d_nCols;
}

inline double &Matrix::el(size_t row, size_t col) const
{
    return d_data[row * d_nCols + col];
}

#endif
