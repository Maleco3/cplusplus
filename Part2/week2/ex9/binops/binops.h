#ifndef INCLUDED_BINOPS_
#define INCLUDED_BINOPS_

#include "../addition/addition.h"
#include "../subtraction/subtraction.h"

class Operations;

class Binops : public Addition, public Subtraction
{
  friend class Addition;
  friend class Subtraction;

  public:
    Binops(Operations *op)
    :Addition(op), Subtraction(op)
    {}

    void addBinop(Operations const &rhs);
    void subBinop(Operations const &rhs);
};

#endif
