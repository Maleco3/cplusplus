#include "addition.ih"

Operations operator+(Operations const &lhs, Operations const &rhs)
{
  Operations tmp(lhs);
  tmp += rhs;
  return tmp;
}
