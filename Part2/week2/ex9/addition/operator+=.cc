#include "addition.ih"

Operations &Addition::operator+=(Operations const &rhs)
{
  d_opAdd->addBinop(rhs); // Binop is used to access private add function
  return *d_opAdd;
}
