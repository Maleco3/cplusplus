#include "subtraction.ih"

Operations  &Subtraction::operator-=(Operations const &rhs)
{
  d_opSub->subBinop(rhs);
  return *d_opSub;
}
