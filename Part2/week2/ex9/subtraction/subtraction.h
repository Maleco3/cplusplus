#ifndef INCLUDED_SUBTRACTION_
#define INCLUDED_SUBTRACTION_

class Operations;

class Subtraction
{
   public:
    Operations *d_opSub;

    Subtraction(Operations *op)
    : d_opSub(op)
    {}

    Operations &operator-=(Operations const &rhs);
};

Operations operator-(Operations const &lhs, Operations const &rhs);

#endif
