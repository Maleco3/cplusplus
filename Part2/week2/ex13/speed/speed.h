#ifndef INCLUDED_SPEED_
#define INCLUDED_SPEED_

#include "../time/time.h"
#include "../coordinates/coordinates.h"

class Speed
{
    Time const &d_time;
    Coordinates &d_coord;
    Units const &d_units;

    double d_reqSpeed = 0;  // requested speed in m/s

    public:
        Speed(Time const &time, Coordinates &coord,
              Units const &units);

        void set(double speed);         // requested speed in m/s
};

#endif
