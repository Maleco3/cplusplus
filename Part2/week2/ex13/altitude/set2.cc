#include "altitude.ih"

void Altitude::set(double altitude)             // m
{
    if (round(altitude) == d_coord.z())
    {
        cerr << "Already at " << d_units.altitude(altitude) << '\n';
        return;
    }

    if (d_reqAlt > Limits::CEILING)
    {
        cerr << 
            "Requested altitude (" << d_units.altitude(altitude) << 
                ") exceeds the ceiling (" << 
                                    d_units.altitude(Limits::CEILING) << "\n"
                "Req. altitude changed to " << 
                                    d_units.altitude(Limits::CEILING) << '\n';
        altitude = Limits::CEILING;
    }
    else
        cerr << "Setting altitude to " << 
                                    d_units.altitude(altitude) << '\n';

    d_reqAlt = altitude;
}






