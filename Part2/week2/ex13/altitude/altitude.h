#ifndef INCLUDED_ALTITUDE_
#define INCLUDED_ALTITUDE_

#include <iosfwd>

class Time;
class Speed;
class Coordinates;
class Units;

class Altitude
{
    Time const &d_time;
    Coordinates &d_coord;
    Speed &d_speed;
    Units const &d_units;

    double d_reqAlt = 0;           // req. alt. (m)
    double d_rate = 0;             // rate of climb/descent (m/s)

    public:
        Altitude(Time const &time, Coordinates &coord, Speed &speed,
                 Units const &units);

        void set(double altitude, double rate);             // 1
        double rate() const;


    private:
        void set(double altitude);                          // 2
        void setRate(double rate);
};

#endif
