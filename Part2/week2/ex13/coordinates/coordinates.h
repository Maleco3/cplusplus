#ifndef INCLUDED_COORDINATES_
#define INCLUDED_COORDINATES_

#include <iosfwd>

class Units;

class Coordinates
{
    Units const &d_units;

    int d_x;                // current coordinate
    int d_y;
    int d_z = 0;            // altitude (m)
                            // negative values are allowed internally, because
                            // during a descent the next d_z value may compute
                            // as negative. Allowing negative values for that
                            // situation is semantically more attractive than
                            // testing for very large positive values.

    int d_xPrev;            // the coordinates at the previous Time-tick
    int d_yPrev;
    int d_zPrev = 0;

    public:
        enum BoxStatus
        {
            STABLE,
            ENTERING,
            LEAVING
        };

                                // for this exercise: locally defining a Units
                                // or maybe even a static Units object is also
                                // OK
        Coordinates(int xCoord, int yCoord, Units const &units);

        void deltaXY(int deltaX, int deltaY);
        void setZ(int nextZ);

        BoxStatus boxCheck() const;
        int x() const;
        int y() const;
        size_t z() const;              // alt. in meters

        bool inTheBox() const;

    private:
        static bool constexpr in(int coord, int minimum, int maximum);
};

inline int Coordinates::x() const
{
    return d_x;
}

inline int Coordinates::y() const
{
    return d_y;
}

inline size_t Coordinates::z() const
{
    return d_z;
}

#endif
