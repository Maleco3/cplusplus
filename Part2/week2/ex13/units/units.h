#ifndef INCLUDED_UNITS_
#define INCLUDED_UNITS_

#include <cmath>
#include <string>
#include <cstddef>

class Units
{
    enum
    {
        SPEED = 0,          // Values explicit because they are array-
        PLAINDIST = 1,      // indices and not merely names: see data.cc
        DISTANCE = 2,
        ALTITUDE = 3,
        CLIMBRATE = 4,
    };

    std::string (**d_unit)(double value) = s_aero;

    static std::string (*s_metric[])(double value);
    static std::string (*s_aero[])(double value);

    public:
        void setUnits(int type);
                                        // entered values are size_t,
                                        // converted to doubles given the
                                        // selected units of measurement
        double setAlt(size_t alt) const;
        double setRate(size_t rate) const;
        double setSpeed(size_t speed) const;

        std::string speed(double m_s) const;
        std::string plainDist(double dist) const;
        std::string distance(double dist) const;
        std::string altitude(double alt) const;
        std::string climbRate(double rate) const;

        static double constexpr kts2m_s(double knots);
        static double constexpr ft2m(double feet);
        static double constexpr ft_min2m_s(int ftmin);
        static int    constexpr nm2m(double nm);
        static int    constexpr km2m(double nm);

        static std::string heading(double rad);     // merely the value
        static std::string degrees(double rad);     // 'value' deg.

        static double constexpr deg2rad(double deg);

    private:
        static std::string metricSpeed(double m_s);     // source is speed.cc
        static std::string aeroSpeed(double m_s);       // (etc)

        static std::string metricPlainDist(double m_s);
        static std::string aeroPlainDist(double m_s);

        static std::string metricDistance(double m_s);
        static std::string aeroDistance(double m_s);

        static std::string metricAltitude(double coord);
        static std::string aeroAltitude(double coord);

        static std::string metricClimbRate(double coord);
        static std::string aeroClimbRate(double coord);


        static double constexpr m_s2kmh(double ms);
        static double constexpr m_s2kts(double ms);
        static double constexpr m2nm(int coord);
        static double constexpr m2ft(double meters);
        static double constexpr m_s2ft_min(double m_s);
        static double constexpr kmh2m_s(double kmh);
};

inline double Units::setAlt(size_t alt) const
{
    return d_unit == s_metric ? alt : ft2m(alt);
}

inline double Units::setRate(size_t rate) const
{
    return d_unit == s_metric ? rate : ft_min2m_s(rate);
}

inline double Units::setSpeed(size_t speed) const
{
    return d_unit == s_metric ? kmh2m_s(speed) : kts2m_s(speed);
}

inline double constexpr Units::kts2m_s(double kts)
{
    return 1.852 / 3.6 * kts;
}

inline double constexpr Units::kmh2m_s(double kmh)
{
    return kmh / 3.6;
}

inline double constexpr Units::ft2m(double feet)
{
    return 3 / 10. * feet;
}

inline void Units::setUnits(int type)
{
    d_unit = type == 'm' ? s_metric : s_aero;
}

inline std::string Units::speed(double m_s) const
{
    return (*d_unit[SPEED])(m_s);
}

inline std::string Units::plainDist(double dist) const
{
    return (*d_unit[PLAINDIST])(dist);
}

inline std::string Units::distance(double alt) const
{
    return (*d_unit[DISTANCE])(alt);
}

inline std::string Units::altitude(double dist) const
{
    return (*d_unit[ALTITUDE])(dist);
}

inline std::string Units::climbRate(double dist) const
{
    return (*d_unit[CLIMBRATE])(dist);
}

// public conversion functions

inline double constexpr Units::ft_min2m_s(int ftmin)
{
    return ftmin / (3. * 60);
}

inline double constexpr Units::deg2rad(double deg)
{
    return M_PI / 180 * deg;
}

inline int constexpr Units::nm2m(double nm)
{
    return round(nm * 1852);
}

inline int constexpr Units::km2m(double km)
{
    return round(km * 1000);
}

#endif
