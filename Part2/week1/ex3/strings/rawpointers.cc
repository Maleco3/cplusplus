// New should throw a bad_alloc exception if it fails,
// so no need to add another throw here. Catching happens in higher up functions.

#include "strings.ih"

string **Strings::rawPointers(size_t nPointers)
{
    return new string *[nPointers];
}
