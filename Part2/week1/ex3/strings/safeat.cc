// If the index is out of bounds, an exception has to be thrown.

#include "strings.ih"

std::string &Strings::safeAt(size_t idx) const
try
{
    if (idx >= d_size)
        throw idx;
    return *d_str[idx];
}
catch (size_t)
{
    cout << "Index out of bounds, returning empty string\n";

    static string empty;
    empty.clear();
    return empty;
}
