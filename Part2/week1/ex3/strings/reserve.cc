// Catch a possible bad_alloc and if so, reset d_capacity te preserve the
// original capacity.

#include "strings.ih"

void Strings::reserve(size_t nextCapacity)
{
    size_t old_cap = d_capacity;
    if (d_capacity < nextCapacity)
    {
        while (d_capacity < nextCapacity)
            d_capacity <<= 1;

        try
        {
            d_str = enlarged();
        }
        catch (bad_alloc)
        {
            cout << "Warning: something when wrong in enlargement, resetting\n";
            d_capacity = old_cap;
        }
    }
}
