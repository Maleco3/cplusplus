// Since the allocation in rawPointers() can fail, make sure that if it does,
// a warning is given and the memory gets freed.

#include "strings.ih"

Strings::Strings()
try
:
    d_str(rawPointers(1))
{}
catch (bad_alloc)
{
    cout << "Warning: object creation failed due to bad allocation\n";
    delete d_str;
}
