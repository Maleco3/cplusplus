// Exceptions may not leave the destructor, therefore no function try block

#include "strings.ih"

Strings::~Strings()
{
    try
    {
        for (; d_size--; )
            delete d_str[d_size];

        destroy();
    }
    catch (...)
    {
        cout << "Warning: something went wrong with deleting an object";
    }
}
