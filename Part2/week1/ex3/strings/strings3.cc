// Allocation can fail in add, and the exception there is thrown further,
// so that we can completely remove the remainder of the object.

#include "strings.ih"

Strings::Strings(char **environLike)
try
:
    Strings()
{
    while (*environLike)
        add(*environLike++);
}
catch (bad_alloc)
{
    cout << "Object creation failed due to bad allocation\n";
    delete d_str;
}
