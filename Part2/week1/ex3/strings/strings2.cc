// Allocation can fail in add, and the exception there is thrown further,
// so that we can completely remove the remainder of the object.

#include "strings.ih"

Strings::Strings(int argc, char *argv[])
try
:
    Strings()
{
    for (size_t begin = 0, end = argc; begin != end; ++begin)
        add(argv[begin]);
}
catch (bad_alloc)
{
    cout << "Object creation failed due to bad allocation\n";
    delete d_str;
}
