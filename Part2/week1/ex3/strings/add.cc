// Both here and in storageArea() new is used so we have to catch Possible
// bad_alloc exceptions. If so, we reset the old size and capacity.
// Also we throw the exception further in case more things have to be taken care
// of, such as in the constructors.

#include "strings.ih"

void Strings::add(string const &next)
{
    size_t old_cap = d_capacity;
    size_t old_size = d_size;

    try
    {
    string **tmp = storageArea();

    tmp[d_size] = new string(next);

    if (tmp != d_str)               // destroy old memory if new storageArea
    {                               // was allocated
        destroy();                  // destroy the old string * array
        d_str = tmp;
    }

    ++d_size;
    }
    catch (bad_alloc)
    {
        cout << "Warning: something when wrong in allocation, resetting\n";
        d_capacity = old_cap;
        d_size = old_size;
        throw;
    }
}
