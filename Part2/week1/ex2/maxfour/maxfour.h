#ifndef INCLUDED_MAXFOUR_
#define INCLUDED_MAXFOUR_

#include <iostream>
#include <string>

class MaxFour
{
  static int counter;
  public:
    MaxFour()
    {
      try
      {
        if (counter++ >= 4)
          throw "max. number of objects reached\n";
        std::cout << "Currently created " << counter << " objects.\n";
      }
      catch (char const *ex)
      {
        std::cout << ex;
        throw;
      }
    }
    ~MaxFour()
    {
      std::cout << "Destructor called" << '\n';
    }

  private:
};
int MaxFour::counter = 0;

#endif
