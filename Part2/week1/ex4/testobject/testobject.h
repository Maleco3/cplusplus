#ifndef INCLUDED_TESTOBJECT_
#define INCLUDED_TESTOBJECT_

#include <string> // to allow inline function definitions with strings
#include <iostream> // to allow inline function definitions with output
using namespace std;

class TestObject
{
  string d_name;

  public:
    TestObject(string name)
    :
      d_name(name)
    {
      cout << "Called constructor of " << d_name << '\n';
    }
    TestObject(TestObject &to)
    :
      d_name(to.d_name + " (copy) ")
    {
      cout << "Called copy constructor of " << d_name << '\n';
    }
    ~TestObject()
    {
      cout << "Called destructor of " << d_name << '\n';
    }

private:
};

#endif
