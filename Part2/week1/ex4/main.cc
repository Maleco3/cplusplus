#include "main.ih"

int main(int argc, char **argv)
{
  try
  {
    TestObject to1("testobject1");
    for (size_t idx = 10; idx--;)
    {
      TestObject to2("testobject2");
      for (size_t idx2 = 10; idx2--;)
      {
        try
        {
          TestObject to3("testobject3");
          if (idx2 == 5)
          {
            cout << "we should terminate\n";
            throw idx2;
          }
        }
        catch (size_t errorCode)
        {
          cout << "Idx2 = " << errorCode << ", let's throw an error\n";
          throw;
        }
      }
    }
  }
  catch (...)
  {
    cout << "Outer catch\n";
    return 1;
  }
  cout << "We should not reach this point\n";
}
