#include "main.ih"

void troublemaker()
{
    throw 1;
}

int main(int argc, char **argv)
{
    ShowExcepts example(5, troublemaker);
    cout << "Example accepted throw in asAthrowList \n--------------\n";
    try
    {
        example.asAthrowList();
    }
    catch (bad_exception)
    {
        cout << "Caught a bad exception, not cool\n";
    }
    catch (...)
    {
        cout << "I should have caught an int or string\n";
    }

    ShowExcepts example2(-1, troublemaker);
    cout << "\nExample non accepted throw in asAthrowList \n--------------\n";
    try
    {
        example2.asAthrowList();
    }
    catch (bad_exception)
    {
        cout << "Caught a bad exception, not cool\n";
    }
    catch (...)
    {
        cout << "I should have caught an int or string\n";
    }

    cout << "\nExample noexcept \n--------------\n";
    example.asNoexcept();
}
