#include "showexcepts.ih"

void ShowExcepts::asAthrowList()
try
{
    d_thrower > 0 ? throw d_thrower
                  : throw 0.0;
}
catch (int)
{
    throw; // Throwing an int is allowed now
}
catch (string)
{
    throw; // Also allowed
}
catch (...)
{
    throw bad_exception(); // Anything else is bad
}
