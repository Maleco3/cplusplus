#include "showexcepts.ih"

void ShowExcepts::asNoexcept()
try
{
    d_fp();
}
catch (...) // AAARGGH this makes me angry!
{
    cout << "How dare you throw stuff at me!\n";
    terminate();
}
