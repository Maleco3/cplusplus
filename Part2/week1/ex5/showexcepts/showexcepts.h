#ifndef INCLUDED_SHOWEXCEPTS_
#define INCLUDED_SHOWEXCEPTS_


class ShowExcepts
{
    int d_thrower;
    void (*d_fp)();

    public:
        ShowExcepts(int number, void (*fp)() );

        void asAthrowList();
        void asNoexcept();

    private:
};

#endif
