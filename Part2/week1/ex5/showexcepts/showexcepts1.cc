#include "showexcepts.ih"

ShowExcepts::ShowExcepts(int number, void (*fp)() )
:
    d_thrower(number),
    d_fp(fp)
{
}
