#include "main.ih"

int main(int argc, char **argv)
{
  try
  {
    TestObject to("testobject");
    TestObject to2("testobject2");
    TestObject to3("testobject3");

    try // Throw and catch an object
    {
      throw to;
    }
    catch (TestObject to)
    {
    }

    try // Throw and catch a reference to an object
    {
      throw to2;
    }
    catch (TestObject &y)
    {
    }

    try // Throw and rethrow
    {
      throw to3;
    }
    catch (TestObject &to)
    {
      cout << "Rethrow\n";
      throw;
    }
  }
  catch (TestObject &to)
  {
    cout << "Outer catch\n";
  }
}
