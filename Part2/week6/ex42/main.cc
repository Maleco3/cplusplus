#include <unistd.h>
#include <iostream>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <iterator>

using namespace std;

int main(int argc, char **argv)
{
  int pipefd[2];
  pipe(pipefd);
  pid_t pid = fork();

  if (pid == 0)     // Child process
  {
    close(pipefd[0]);
    dup2(pipefd[1], STDOUT_FILENO);
    execl("/bin/ls", "ls", "-al", (char *)NULL);
  }
  else // Parent process
  {
    close(pipefd[1]);
    dup2(pipefd[0], STDIN_FILENO);
    wait(NULL);
    size_t numLines = 0, numChars = 0;
    string line;
    while (getline(cin, line))
    {
        ++numLines;
        for (char x : line)
            if (!isspace(x))
                ++numChars;

    }
    cout << "Number of lines: " << numLines << endl;
    cout << "Number of chars: " << numChars << endl;
  }
}
