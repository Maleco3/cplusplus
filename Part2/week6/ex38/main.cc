#include "main.ih"

int main(int argc, char **argv)
{
  ComputeFilesize(argv[1]).run(); // compute the total file size of argv[1]
}
