#include "visitfiles.ih"
typedef std::chrono::system_clock sc;

void VisitFiles::run()
{
  //=========================== EX 39 ==============================
  auto startTimeT = sc::to_time_t(d_startTp);
  cout << "Starting time:\t" << put_time(localtime(&startTimeT), "%c") << '\n';

  // Start a thread that processes the files
  thread thr(&VisitFiles::visitdirs, this);
  thr.detach();

  // Display 1 '.' per second while waiting on the thread to finish
  unique_lock<mutex> lock (d_mutex);
  while(d_cv.wait_for(lock,chrono::seconds(1)) == cv_status::timeout)
  {
    cout << '.' << flush; // Flush the buffer to print every second
  };

  //=========================== EX 39 ==============================
  auto endTime = sc::now();
  auto endTimeT = sc::to_time_t(endTime);
  auto duration = chrono::duration_cast<chrono::milliseconds>
                    (sc::now() - d_startTp);

  cout << '\n'
       << "It is now:\t" << put_time(localtime(&endTimeT), "%c") << '\n'
       << "Time needed:\t" << duration.count() << " ms\n";

  // Report the results
  report();
}
