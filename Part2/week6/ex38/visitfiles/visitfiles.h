#ifndef INCLUDED_VISITFILES_
#define INCLUDED_VISITFILES_

#include <dirent.h>
#include <mutex>
#include <condition_variable>

typedef std::chrono::system_clock sc;

class VisitFiles
{
  std::mutex d_mutex;
  std::condition_variable d_cv;

  std::string d_dirname;
  sc::time_point d_startTp;

  public:
    VisitFiles(std::string dirname)
      :
      d_dirname(dirname),
      d_startTp(sc::now())
    {
    }

    void run(); // Runs the actual visiting program (with threading and reporting)

  private:
    // Function that processes a file, implementation-specific
    virtual void process(struct dirent *rdir) = 0;
    // Display the results of the process
    virtual void report() = 0;

    void visitdirs();
    void visitdir(std::string dirname);
    void visitdirfiles(DIR *dirp);
};

#endif
