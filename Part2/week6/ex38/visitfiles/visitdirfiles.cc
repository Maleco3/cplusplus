#include "visitfiles.ih"

void VisitFiles::visitdirfiles(DIR *dirp)
{
  struct dirent *rdir;
  while((rdir = readdir(dirp)))
  {
    // Using string compare, since strcmp would also skip hidden files
    if(!string(rdir->d_name).compare(".") | !string(rdir->d_name).compare(".."))
      continue;

    process(rdir);

    // Check if the current file is a directory, if so enter recursion
    struct stat fileStat;
    lstat(rdir->d_name, &fileStat);
    if(S_ISDIR(fileStat.st_mode))
      visitdir(rdir->d_name);
  }
}
