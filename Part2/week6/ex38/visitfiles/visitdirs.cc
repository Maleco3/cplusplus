#include "visitfiles.ih"
#include <iostream>
void VisitFiles::visitdirs()
{
  // Visit and process all the files
  visitdir(d_dirname);
  // Signal via the condition_variable that we are done visiting
  d_cv.notify_all();
}
