#include "visitfiles.ih"

void VisitFiles::visitdir(std::string dirname)
{
  try
  {

    DIR *dirp;
    if((dirp  = opendir(dirname.c_str())) == NULL)
      throw strerror(errno);

    if (chdir(dirname.c_str()) != 0)
      throw strerror(errno);

    visitdirfiles(dirp);

    closedir(dirp);

    if (chdir("..") != 0)
      throw strerror(errno);
  }
  catch (char *error)
  {
    // cerr << error<< '\n';
    return;
  }
}
