#ifndef INCLUDED_COMPUTEFILESIZE_
#define INCLUDED_COMPUTEFILESIZE_

#include "../visitfiles/visitfiles.h"
#include <mutex>

class ComputeFilesize : public VisitFiles
{
  size_t d_size = 0; // The total size of all the reg files inside dirname

  public:
    ComputeFilesize(std::string dirname)
      :
      VisitFiles(dirname)
    {
    }

  private:
    void process(struct dirent *rdir);
    void report();
};

#endif
