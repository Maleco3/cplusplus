#include "computefilesize.ih"
#include <iostream>
void ComputeFilesize::process(struct dirent *rdir)
{
  // Get the lstat of the file (We are already in the dir)
  struct stat fileStat;
  lstat(rdir->d_name, &fileStat);
  if(S_ISREG(fileStat.st_mode))
    d_size += fileStat.st_size;
}
