#ifndef INCLUDED_SEMAPHORE_
#define INCLUDED_SEMAPHORE_

#include <iosfwd>
#include <mutex>
#include <condition_variable>

class Semaphore
{
  mutable std::mutex d_mutex;
  std::condition_variable d_condition;
  size_t d_available;

  public:
    Semaphore(size_t nAvailable);

    void notify();
    void notify_all();
    size_t size() const;
    void wait();
  private:
};

inline size_t Semaphore::size() const
{
  return d_available;
}

#endif
