#include "safequeue.ih"

size_t SafeQueue::size()
{
    return d_queue.size();
}
