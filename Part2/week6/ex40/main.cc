#include "main.ih"

int main(int argc, char **argv)
{
    SafeQueue sq;
    // sq.push(1);
    // sq.push(2);
    thread th4(&SafeQueue::pop, &sq);
    thread th3(&SafeQueue::pop, &sq);
    thread th2(&SafeQueue::push, &sq, 3);
    thread th1(&SafeQueue::front, &sq);
    thread th5(&SafeQueue::push, &sq, 7);
    thread th6(&SafeQueue::push, &sq, 8);
    thread th7(&SafeQueue::front, &sq);
    th1.join();
    th2.join();
    th3.join();
    th4.join();
    th5.join();
    th6.join();
    th7.join();
    // cout << sq.back() << endl;
}
