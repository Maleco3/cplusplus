#include "main.ih"

int main(int argc, char **argv)
{
    int length = 1000000;
    int test[length+1];
    for (size_t idx = length; --idx;){
        test[idx] = rand();
    }
    Qsort(test, test + 200);
}
