#ifndef INCLUDED_CERREXTRACTOR_
#define INCLUDED_CERREXTRACTOR_

#include <iosfwd>
#include "../fork/fork.h"
#include "../pipe/pipe.h"

class CerrExtractor : protected Fork
{
  size_t d_bufSize;
  Pipe d_pipe;

  public:
    CerrExtractor(size_t bufSize = 100);

    bool execute(std::string const &cmd);

  private:
};

#endif
