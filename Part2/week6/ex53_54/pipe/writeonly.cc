#include "pipe.ih"

int Pipe::writeOnly()
{
  close(d_pipefd[READ]);
  return d_pipefd[WRITE];
}
