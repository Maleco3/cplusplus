#include "pipe.ih"

void Pipe::redirect(int d_fd, int alternateFd)
{
  if (dup2(d_fd, alternateFd) < 0)
    throw "Pipe: redirection failed";
}
