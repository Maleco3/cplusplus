#include "pipe.ih"

Pipe::Pipe()
{
  if (pipe(d_pipefd))
    throw "Pipe::Pipe(): pipe() failed";
}
