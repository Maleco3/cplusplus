#include "pipe.ih"

int Pipe::readOnly()
{
  close(d_pipefd[WRITE]);
  return d_pipefd[READ];
}
