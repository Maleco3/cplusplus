#ifndef INCLUDED_PIPE_
#define INCLUDED_PIPE_

#include <unistd.h>

class Pipe
{
  enum RW { READ, WRITE };
  int d_pipefd[2];

  public:
    Pipe();

    int readOnly();
    int writeOnly();

    void redirect(int d_fd, int alternateFd);
    void writtenBy(int fileDescriptor);
    
  private:
};

#endif
