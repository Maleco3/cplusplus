#ifndef INCLUDED_FORK_
#define INCLUDED_FORK_


class Fork
{
  int d_pid;

  public:
    virtual ~Fork();
    void fork();

  protected:
    int pid() const;
    int waitForChild();

  private:
    virtual void childRedirections();
    virtual void parentRedirections();
    virtual void childProcess() = 0;
    virtual void parentProcess() = 0;
};

inline int Fork::pid() const
{
  return d_pid;
}

#endif
