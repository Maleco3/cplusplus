#ifndef INCLUDED_SAFEQUEUE_
#define INCLUDED_SAFEQUEUE_

#include <iostream>
#include <queue>
#include <thread>
#include <mutex>
#include <condition_variable>

class SafeQueue
{
    std::queue<int> d_queue;

    mutable std::mutex d_mutex;
    std::condition_variable d_condition;

    public:
        class Proxy;
        Proxy front();
        Proxy back();

        class Proxy
        {
            friend Proxy SafeQueue::front();
            friend Proxy SafeQueue::back();

            int d_val;
            SafeQueue *d_parent;
            Proxy(int val, std::mutex &mut, SafeQueue *sq);

            public:
                ~Proxy();

                int &operator=(int const &rhs);
                operator int const &() const;
        };

        void push(int val);
        void pop();
};

inline SafeQueue::Proxy::Proxy(int val, std::mutex &mut, SafeQueue *sq)
:
    d_val(val),
    d_parent(sq)
{
    std::unique_lock<std::mutex> lk(mut);
}

inline SafeQueue::Proxy::~Proxy()
{
    d_parent->pop();
}

inline int &SafeQueue::Proxy::operator=(int const &rhs)
{
    return d_val = rhs;
}

inline SafeQueue::Proxy::operator int const &() const
{
    return d_val;
}

#endif
