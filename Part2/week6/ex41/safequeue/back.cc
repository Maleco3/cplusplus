#include "safequeue.ih"

SafeQueue::Proxy SafeQueue::back()
{
    unique_lock<mutex> lk(d_mutex);
    while (d_queue.size() == 0)
        d_condition.wait(lk);
    lk.~unique_lock();
    return Proxy(d_queue.back(), d_mutex, this);
};
