#include "main.ih"

typedef packaged_task<double (double*, double*, size_t)> Task;

double lhs[4][5];
double rhsT[6][5];
future<double> fut[4][6];

double innerProduct(double *one, double *two, size_t size)
{
    double sum = 0;
    for (size_t idx = 0; idx != size; ++idx)
        sum += *(one + idx) * *(two + idx);

    return sum;
}

void compute()
{
    for (size_t row = 0; row != 4; ++row)
        for (size_t col = 0; col != 6; ++col)
        {
            Task task(innerProduct);
            fut[row][col] = task.get_future();
            thread(move(task), lhs[row], rhsT[col], 5).detach();
        }
}

int main(int argc, char **argv)
{
    for (size_t col = 0; col != 5; ++col)
    {
        for (size_t row = 0; row != 4; ++row)
            lhs[row][col] = row * col;
        for (size_t row2 = 0; row2 != 6; ++row2)
            rhsT[row2][col] = row2*col;
    }
    compute();
    for (size_t row = 0; row != 4; ++row)
    {
        for (size_t col = 0; col != 6; ++col)
            cout << fut[row][col].get() << ' ';
        cout << '\n';
    }
}
