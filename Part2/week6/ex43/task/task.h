#ifndef INCLUDED_TASK_
#define INCLUDED_TASK_

#include <iosfwd>
#include <string>

class Task
{
  int (*d_task)(int);
  char const *d_taskLabel;
  std::string d_filename = "";
  size_t d_count = 0;
  bool d_stop = false;

  public:
    Task(int (*fn)(int), char const *taskLabel);

    size_t count();
    void operator()();
    void run();
    void setFilename(char const *name);
    void stopThread();
  private:
};

inline size_t Task::count()
{
  return d_count;
}

inline void Task::setFilename(char const *name)
{
  d_filename = name;
}

inline void Task::stopThread()
{
  d_stop = true;
}

#endif
