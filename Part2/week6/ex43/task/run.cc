#include "task.ih"

void Task::run()
{
  ifstream ifs(d_filename, ifstream::in);
  char x;
  while(ifs.good())
  {
    x = ifs.get();
    if(d_task(x))
      ++d_count;
  }
}
