#include "task.ih"

void Task::operator()()
{
  while(d_filename == "")
  {
    if (d_stop)
      return;
    std::this_thread::yield();
  }
  run();
}
