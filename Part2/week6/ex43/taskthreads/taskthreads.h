#ifndef INCLUDED_TASKTHREADS_
#define INCLUDED_TASKTHREADS_

#include <thread>
#include "../task/task.h"

int isPunct(int ch);
int isVowel(int ch);

class TaskThreads
{
  enum
  {
      VOWELS = 0,
      DIGITS = 1,
      HEXA = 2,
      PUNCT = 3
  };

  bool d_sequentially = false;
  char const *d_filename;
  size_t d_vowels = 0;
  size_t d_digits = 0;
  size_t d_hexadecimals = 0;
  size_t d_punctuations = 0;

  Task d_tasks[4] =
  {
      Task(isVowel, "vowels"),
      Task(isdigit, "digits"),
      Task(isxdigit, "hexadecimals"),
      Task(isPunct, "punctuation")
  };

  std::thread d_threads[4] =
  {
      std::thread(std::ref(d_tasks[VOWELS])),
      std::thread(std::ref(d_tasks[DIGITS])),
      std::thread(std::ref(d_tasks[HEXA])),
      std::thread(std::ref(d_tasks[PUNCT]))
  };

  public:
    TaskThreads(int argc, char **argv);
    void run();
    void countSeq();
    void printOutput();
  private:
};

#endif
