#include "taskthreads.ih"

void TaskThreads::run()
{
  for (auto &th : d_threads)
    th.join();

  if (d_sequentially)
    countSeq();
  else
  {
    d_vowels = d_tasks[VOWELS].count();
    d_digits = d_tasks[DIGITS].count();
    d_hexadecimals = d_tasks[HEXA].count();
    d_punctuations = d_tasks[PUNCT].count();
  }
  printOutput();
}
