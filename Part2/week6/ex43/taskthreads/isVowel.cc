int isVowel(int ch)
{
  switch(ch)
  {
      case 'a':
      case 'e':
      case 'i':
      case 'o':
      case 'u':
      case 'A':
      case 'E':
      case 'I':
      case 'O':
      case 'U':
        return 1;
    }
    return 0;
  }
