#include "taskthreads.ih"

void TaskThreads::countSeq()
{
  ifstream ifs(d_filename, ifstream::in);
  char x;
  while(ifs.good())
  {
    x = ifs.get();
    if(isVowel(x))
      ++d_vowels;
    if(isdigit(x))
      ++d_digits;
    if(isxdigit(x))
      ++d_hexadecimals;
    if(isPunct(x))
      ++d_punctuations;
  }
}
