#include "taskthreads.ih"

TaskThreads::TaskThreads(int argc, char **argv)
:
  d_filename(argv[1])
{
  if (argc > 2)
  {
    cout << "Counting sequentially\n";
    d_sequentially = true;
    for (Task &task : d_tasks)
      task.stopThread();
  }
  else
  {
    cout << "Counting parallel\n";
    for (Task &task : d_tasks)
      task.setFilename(argv[1]);
  }
}
