#include "main.ih"

typedef packaged_task<double ()> Task;

double lhs[4][5];
double rhsT[6][5];
future<double> fut[4][6];
queue<Task> tasks;

double result[4][6];
size_t workers = 8;

mutex jobMutex;
size_t jobCounter = 0;
mutex readyMutex;
size_t readyCounter;
mutex mReady;
condition_variable cv;

double innerProduct(double *one, double *two)
{
    double sum = 0;
    for (size_t idx = 0; idx != 5; ++idx)
        sum += *(one + idx) * *(two + idx);
    return sum;
}

void compute()
{
    for (size_t row = 0; row != 4; ++row)
        for (size_t col = 0; col != 6; ++col)
        {
            Task task(bind(innerProduct, lhs[row], rhsT[col]));
            fut[row][col] = task.get_future();
            tasks.push(move(task));
        }
}


void worker()
{
    while(!tasks.empty())
    {
        cout << "getting a job\n";
        // Get a new job
        jobMutex.lock();
        Task task = move(tasks.front());
        tasks.pop();
        jobMutex.unlock();

        task();

        if (tasks.empty())
          cv.notify_one();
        cout << "Still not done\t" << tasks.size() << "\n";

    }
}

int main(int argc, char **argv)
{
    // Fill the matrices
    for (size_t col = 0; col != 5; ++col)
    {
        for (size_t row = 0; row != 4; ++row)
            lhs[row][col] = 5;
        for (size_t row2 = 0; row2 != 6; ++row2)
            rhsT[row2][col] = 5;
    }
    compute();                            // Create the tasks

    for (size_t idx = workers; idx--;)     // Start the workers
      thread(worker).detach();

    unique_lock<mutex> lck(mReady);           // Wait for the workers to finish
    cv.wait(lck);

    for (size_t row = 0; row != 4; ++row)
    {
        for (size_t col = 0; col != 6; ++col)
            cout << fut[row][col].get() << ' ';
        cout << '\n';
    }
}
