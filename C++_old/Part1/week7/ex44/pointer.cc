void strcpy (char *dst, char const *src)
{
	while ((*dst++ = *src++))
		;
}
