#include "main.ih"

Action encryptOrDecrypt (char* argument)
{
	string arg = argument;
	if (arg == "-e")
		return Action::ENCRYPT;
	else if (arg == "-d")
		return Action::DECRYPT;	
	return Action::UNKNOWN;
}
