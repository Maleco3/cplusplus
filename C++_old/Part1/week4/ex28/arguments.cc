#include "main.ih"

bool arguments(int const &argc, string const &progname) 
{
	if (argc > 1)
		return true;
	usage(progname);
	return false;
}
