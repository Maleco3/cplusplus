#include "main.ih"

void usage(string const &progname)
{
	cout << "\n" <<
		progname << " by " << "Maikel Withagen" << "\n" <<
		progname << " V" << "0.1" << ' '  << 2014 << "\n"
		"\n"
		"Usage: " << progname << " option < infile > outfile\n"
		"Where:\n"
		"   option - select from:\n"
		"      -e      - encrypt infile, writing outfile\n"
		"      -d      - decrypt infile, writing outfile\n"
		"   infile  - file read from the standard input stream\n"
		"   outfile - file written to the standard output stream\n"
		"Infile may contain the standard printable ascii characters and '\\n'\n"
		"\n";
}
