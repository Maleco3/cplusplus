#include <iostream>
#include <string>

int funcInt()
{
	return 5;
}

std::string funcString()
{
	return "String";
}

int main()
{
	int &x = funcInt();
	int const &y = funcInt();
	std::string &z = funcString();
	std::string const &a = funcString();
}
