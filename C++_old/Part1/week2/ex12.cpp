#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
	string lop(argv[1]);        // the left-hand operand    
	string operation(argv[2]);  // the operation to perform
	string rop(argv[3]);        // the right-hand operand 

	cout << 
		// If plus
		(operation == "+" ? to_string( stold(lop) + stold(rop)) + '\n' :
		 // If minus
		 (operation == "-" ? to_string( stold(lop) - stold(rop) ) + '\n' :
			// Alternative
			"operation not implemented\n"
		 )
		);
}
