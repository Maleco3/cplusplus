#include <iostream>
#include <string>

using namespace std;

int main()
{
	// Read in the string
	string line;
	getline(cin, line);
	// Strip string of blanks (spaces and tabs)
	line = line.substr(
			line.find_first_not_of(" \t"),
			(line.find_last_not_of(" \t")-line.find_first_not_of(" \t")+1 )
			);
	// Output the string in required format
	cout << '`' << line << "'\n";
}
