#include <iostream>
using namespace std;

int main()
{
	// Read in the value
	size_t value;
	cin >> value;

	// Print the value 
	cout << "unsigned value: " << value 
		<< ", as character: `" << static_cast<char>(value) << "\'\n";

	// Save the value in a char, and print again
	char ch = value; 
	cout << "unsigned value: " 
		<< static_cast<size_t>(static_cast<unsigned char>(ch) ) 
		<< ", as character: `" << ch << "'\n";
}
