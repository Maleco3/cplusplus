#include <iostream>
#include <iomanip>

using namespace std;

int main()
{
	// Read in the values
	double degrees, minutes,seconds;
	char hemisphere;
	cin >> degrees, cin >> minutes, cin >> seconds, cin >> hemisphere;

	// Calculate the value
	double compValue = (hemisphere == 'N' || hemisphere == 'E') ? 
		degrees + (minutes*60 + seconds) / 3600 :
		(degrees + (minutes*60 + seconds) / 3600)*-1;
	 
	// Output the calculated value, with 4 decimal places
	cout << fixed << setprecision(4) <<compValue  << '\n';
}
