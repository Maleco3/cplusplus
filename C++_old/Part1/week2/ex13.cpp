#include <iostream>
using namespace std;

int main(int argc, char *argv[])
{
	string line;
	
	// Read file, check for EOF (meaning no \n encountered) 
	// and print matching sentence 
	cout << (getline(cin, line) && cin.eof() ? 
		 "incomplete line\n" : "complete line\n"
		);
}
