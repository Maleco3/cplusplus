void stringsSwap(Strings &one, Strings &two)
{
    // Store the content of string one
    Strings temp = one;
    // Change the content of string one
    one = two;
    // Change the content of string two
    two = temp;        
    // Contents are swapped
}
