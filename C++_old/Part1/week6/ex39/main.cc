#include "main.ih"

extern char **environ; 

int main(int argc, char **argv)
{
	Strings argr(argv);
	Strings envir(environ);
	argr.add(envir.at(4));
	for (size_t idx = 0; idx != argr.strings(); ++idx)
		cout << argr.at(idx) << '\n';
}
