#include "strings.ih"

Strings::Strings(char **env)
// Constructor for the environ call
{
	int idx = 0;
	while (char *cp = env[idx])
	{
		add(cp);
		++idx;
	}
}
