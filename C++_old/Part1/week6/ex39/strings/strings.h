#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <string>

class Strings
{
	private: 
		std::string *d_str;
		size_t d_size = 0;

	public:
		Strings(int, char **); //1
		Strings(char **);	//2

		size_t strings() const;
		std::string const &at(size_t) const &; // 1
		std::string& at(size_t) &; //2
		void add(char *);	//1
		void add(std::string); //2
};
        
#endif
