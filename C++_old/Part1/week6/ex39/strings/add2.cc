#include "strings.ih"

void Strings::add(string input)
{
	// Create new array
	string *newPlace = new string[d_size+1];
	
	// Copy old data into new location
	for (unsigned int idx = 0; idx < d_size; idx++)
		newPlace[idx] = d_str[idx];
	
	// Copy new string into new location
	newPlace[d_size] = input;

	// Update class data
	d_size++;
	d_str = newPlace;
}
