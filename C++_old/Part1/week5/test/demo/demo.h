#ifndef INCLUDED_DEMO_
#define INCLUDED_DEMO_

class Demo
{
    public:
        Demo();
				void run() &;	//1
				void run() const &; //2
				void run() &&; //3
			
};
#endif
