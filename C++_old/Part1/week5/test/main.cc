#include "main.ih"

int main()
{
	Demo standardDemo;
	Demo const constantDemo;

	// Outputs "run"
	standardDemo.run();
	// Outputs "run"
	caller(standardDemo);
	
	// Outputs "const run"
	constantDemo.run();
	// Outputs "const run"
	caller(constantDemo);

	// Outputs "anonymous run"
	Demo().run();
	// Outputs "anonymous run"
	caller(Demo());
}
