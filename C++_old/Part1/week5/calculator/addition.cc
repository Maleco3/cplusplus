#include "calculator.ih"

void Calculator::addition(int &left, int &right)
{
	// Get the numbers
	getNumber(left);
	getNumber(right);
	// Calculate the result
	d_value = left + right;
}
