#ifndef INCLUDED_CALCULATOR_
#define INCLUDED_CALCULATOR_


class Calculator
{
	Tokenizer d_tokenizer;
	bool d_ok = true;
	int d_result;

    public:
        Calculator(){d_tokenizer;
				void run();

    private:
				void multiply(int &, int &);
				void addition(int &, int &);
				void subtract(int &, int &);
				void display(bool, int);
				void binop (int &, int &);
};
        
#endif
