#include "calculator.ih"

void Calculator::run()
{
	while(true)
	{

		cout << "? ";
		switch(d_tokenizer.next())
		{
			int v1;
			int v2;

			case '*':
				multiply(v1, v2);
				break;
			case '+':
				addition(v1, v2);
				break;
			case '-':
				subtract(v1, v2);
				break;
			// Fall through
			case Token::STOP:	
			case Token::QUIT:
				return;
			case Token::EOLN:
				break;
			// If unknown character is found
			default:
				d_ok = false;
		}
		// Display syntax error or result
		display()
	}
}
