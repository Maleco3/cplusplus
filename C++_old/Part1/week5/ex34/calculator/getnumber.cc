#include "calculator.ih"

// Read in the next number
void Calculator::getNumber(int &number)
{
	bool negative = false;
	while (d_tokenizer.next() == '-')
		// Flip the bool
		negative = !negative;

	// Check if we have number
	if (d_tokenizer.token() != Token::NUMBER)
		d_ok = false;

	(negative ? 
	 	number = d_tokenizer.number() :
		number = -1*d_tokenizer.number();
	)


}
