#include "calculator.ih"

void Calculator::addition(int &left, int &right)
{
	// Get the numbers
	getNumber(left);

	// Is first number correct?
	if(!d_ok)
		return;

	// Try to get second number
	getNumber(right);
	
	// Calculate the result if there are 2 numbers
	// Set result to minus the first if only one number
	if (d_ok)
		d_value = left - right;
	else
	{
		d_value = -1 * left;
		d_ok = true;
	}

}
