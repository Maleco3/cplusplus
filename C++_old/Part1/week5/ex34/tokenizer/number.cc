//int number() returns the value of the last retrieved number 
//(its use is valid after next has returned NUMBER). 

#include "tokenizer.ih"

int tokenizer::number() const
{
	return lastNumber;
}
