// bool skipLine() skips all tokens through EOLN (returning true) 
// or the end of the input stream (returning false). 

#include "tokenizer.ih"

bool tokenizer::skipLine()
{
	if (nextToken == Token::EOLN)
		return true;
	else if (nextToken == Token::STOP)
		return false;
}

