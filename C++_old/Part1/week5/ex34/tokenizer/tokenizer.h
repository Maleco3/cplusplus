#ifndef INCLUDED_TOKENIZER_
#define INCLUDED_TOKENIZER_

#include <iostream>
#include <string>

enum class Token {
	NUMBER = 256, 
	QUIT = 257, 
	STOP = 258, 
	EOLN = 259, 
	UNKNOWN = 260
};

class tokenizer
{
	// The input stream
	std::istream &d_in;
	// The recognized operator characters
	std::string const d_recOpChars = "()+-*/%";
	// The last number found
	int d_lastNumber = 0;
	// The token from the last next call
	Token d_nextToken = Token::UNKNOWN;

    public:
        tokenizer();
				int next();
				int number() const;
				int token() const;
				bool skipLine();
				bool skipBlanks();

    private:
				void setNextToken (Token);
				void setNumberFound (int);
};
        
#endif
