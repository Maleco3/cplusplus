//#int next() returns the next token as an int. 
//The next token is either one of the constants of the enum Token or a character-token, like '+'. 
#include "tokenizer.ih"

int tokenizer::next()
{
	int nextToken;
	d_in >> nextToken;

	// If token, set nextToken
	if (nextToken > 255)
		setNextToken(static_cast<Token>(nextToken));
	return nextToken;
}
