//int token() returns the token returned at the last next call

#include "tokenizer.ih"

int tokenizer::token() const
{
	return nextToken;
}
