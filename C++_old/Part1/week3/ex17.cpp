#include <iostream>
#include <string>

extern char **environ;
using namespace std;

int main (int argc, char *argv[])
{
	// Loop through all the environ variables
	for (int counter = 0; environ[counter] != 0; counter++)
	{
		string env = environ[counter];

		// Split the key:value pairs into substrings
		string key = env.substr(0, env.find('='));
		string value = env.substr(env.find('=')+1, env.length() );

		cout << "Variable `" << key << "\' has value `" << value << "\'.\n";
	}
}
