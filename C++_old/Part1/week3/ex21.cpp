#include <iostream>
#include <string>

using namespace std;

int main()
{
	char op;
	string buffer;
	int v1,v2;
	cout << "? ";
	while (cin.get(op) )
	{
		switch(op)
		{
			case 'q':
				return 0;
				break;
			case '+':
				cin >> v1;
				cin >> v2;
				cout << v1+v2 << '\n';
				break;
			case '*':
				cin >> v1;
				cin >> v2;
				cout << v1*v2 << '\n';
				break;
			case '-':
				cin >> v1;
				if (cin.peek() != '\n')
				{
					cin >> v2;
					cout << v1-v2 << '\n';
				}
				else 
					cout << -1*v1 << '\n';
				break;
			case ' ':
				break;
			case '\n':
					cout << "? ";
				break;
			default:
				cout << "ignoring " << op << "...\n? ";
				getline(cin, buffer);
		}
	}
}



