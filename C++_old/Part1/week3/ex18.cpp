#include <iostream>
#include <string>

using namespace std;

int main (int argc, char *argv[])
{
	int counter = 0;
	string buffer;

	if (argc == 2 && (string)argv[1] == "ok")
	{
		while (getline(cin, buffer))
			counter++;
	} 
	else 
	{
		while (!cin.eof())
		{
			getline(cin, buffer);
			counter++;
		}
	}
	cout << "# of lines: " << counter << "\n";
}
