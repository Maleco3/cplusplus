#include <iostream>
#include <iomanip>

using namespace std;

int main (int argc, char *argv[])
{
	for (int combination = 0; combination != (1<<(argc-1)); combination++)
	{
		cout << combination+1 << ":";
		for (int argument = 0; argument != argc-1; argument++)
		{
			string arg = argv[argument+1];
			((combination & (1<<argument)) > 0) ?
				cout << " " << arg :
				cout << " " << setw(arg.length()) << ' ';
		}
		cout << '\n';
	}
}
