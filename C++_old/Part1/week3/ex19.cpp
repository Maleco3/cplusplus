#include <iostream>
#include <string>

using namespace std;

int main (int argc, char *argv[])
{
	string in;
	while (getline(cin, in) )
	{
		//Print the line if non-empty
		if (in.size() != 0)
			cout << in << '\n';
		
		//Check if next line is empty
		char c = cin.peek();
		if (c != '\n')
		{
			//Keep reading empty lines
			while (c == '\n')
				c = cin.peek();
		}
		//Check if eof is reached
		if(cin.eof() )
			break;
	}
}
