#include<iostream>
#include<ctime>
#include <string>

using namespace std;

ostream &now (ostream &stream)
{
	// Get the current time
	time_t timeNow = time(NULL);
	string result = asctime(localtime(&timeNow));
	// Remove last character (newline)
	result.pop_back();

	return stream << asctime(localtime(&timeNow));
}


int main()
{
	cout << now << '\n' << "Newline" << '\n';
}

//	Ouput:
//	Fri Oct 31 12:20:38 2014
//	Newline
