#include <iostream>
#include <iomanip>
using namespace std;

int main()
{
	double value = 12.05;	
	int initPrecision = cout.precision();

	cout 
		<< setw(15) << value << '\n' 

		<< setw(15) << left  << value << '\n' << resetiosflags(ios::left) 

		<< setw(15) << right << value << '\n' << resetiosflags(ios::right)

		<< setw(15) << setprecision(3) << value << '\n'

		<< setw(15) << fixed << setprecision(4) << value << '\n'

		<< setw(15) << setprecision(initPrecision) 
		<< resetiosflags(ios::fixed) << value	<< '\n' 
		;
}
