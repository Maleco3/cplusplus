#include<iostream>
#include<fstream>

using namespace std;

int main(int argc, char **argv)
{
	string fileName = argv[1];
	ifstream in(fileName, ios::binary);
	
	//determine how many strings are stored in the input file (argv[1])
	size_t nStrings;
	in.read(reinterpret_cast<char *>(&nStrings), sizeof(nStrings));
	cout << "The file contains " << nStrings << " strings\n";

	while (true)
	{
		cout << "? ";
		... get the idx of the next string to show, or stop
			... show the requested string at cout
	}
}
