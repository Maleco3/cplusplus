// One-size-fits-all, variadic function
void fun(...){}

int main()
{
    fun();
    fun("with functions");
    fun(1, 2, 3);
}
   
