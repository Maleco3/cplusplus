#include<iostream>
#include<fstream>

using namespace std;

int main(int argc, char **argv)
{
	if (argc == 1)
		return 0;

	for (size_t begin = 1, end = argc; begin != end; ++begin)
	{
		ifstream in(argv[begin]);
		ofstream out((string)argv[begin] + ".bak");
		out << in.rdbuf();
	}
}
