#include <iostream>
#include <unistd.h>
#include <string>
#include <fstream>

using namespace std;

void redirect()
{
	string line;
	while (true)
	{
		getline(cin, line);
		if(!cin)
			break;
		cout << line << '\n';
	}
}

void interactive()
{
	string line;
	string prompt = "? ";
	while (true)
	{
		cout << prompt;
		getline(cin,line);
		if (line == "quit")
			break;
		cout << line << '\n';
	}
}

void printContent (char const *fileName)
{
	// Copy the entire stream to cout
	ifstream in(fileName);
	cout << in.rdbuf();
}

int main (int argc, char *argv[])
{
	if (argc == 1)
	isatty(STDIN_FILENO) == 0 ?
		redirect() :
		interactive() ;
	else 
		printContent(argv[1]);
}

