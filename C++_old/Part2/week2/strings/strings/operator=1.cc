#include "strings.ih"

Strings &Strings::operator=(Strings const &rvalue)
{
	Strings tmp(rvalue);
	return *this = move(tmp);
}
