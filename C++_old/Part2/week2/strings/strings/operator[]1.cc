#include "strings.ih"

string &Strings::operator[](size_t index)
{
	return safeAt(index);
}
