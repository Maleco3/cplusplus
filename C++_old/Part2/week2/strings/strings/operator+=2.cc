#include "strings.ih"

Strings &Strings::operator+=(Strings const &right)
{
	Strings tmp(*this);
	tmp.addStrings(right);
	swap(tmp);
	return *this;
}
