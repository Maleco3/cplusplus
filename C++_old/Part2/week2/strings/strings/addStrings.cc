#include "strings.ih"

void Strings::addStrings(Strings const &other)
{
	for(size_t idx = 0; idx != other.d_size; ++idx)
		add(other[idx]);
}
