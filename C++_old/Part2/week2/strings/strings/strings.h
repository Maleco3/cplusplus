#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <iosfwd>

class Strings
{
	std::string *d_str;
	size_t d_size;

	public:
	Strings();
	Strings(int argc, char *argv[]);
	Strings(char *environLike[]);
	// exercise 11
	// ===========
	explicit Strings(char *argv); //6: Make promotion from char * to Strings possible

	// Required move- and copy-construction and assignment functions
	Strings(Strings const &other);	// 4 copy constructor
	Strings(Strings &&tmp);	// 5 move constructor
	~Strings();
	Strings & operator=(Strings const &rvalue);	// 1 copy assignment
	Strings & operator=(Strings &&rvalue);// 2 move assignment

	// exercise 10
	// ===========
	std::string &operator[](size_t index);	// 1: value assignment
	std::string const &operator[](size_t index) const; // 2: value retrieval

	// exercise 11
	// ===========
	void operator+=(std::string const &right); 	// 1: add a string
	Strings &operator+=(Strings const &right); 	// 2: add a Strings object

	void add(std::string const &next);          // add another element
	void addStrings(Strings const &other); // add another Strings element

	size_t size() const;  

	void swap(Strings &other);

	private:
	std::string &safeAt(size_t idx) const;      // private backdoor
	std::string *duplicateAndEnlarge();
	void destroy();                 
};

// exercise 11
// ===========
inline Strings const operator+(Strings &left, Strings &right)
{
	return (left += right);
}

inline size_t Strings::size() const         // potentially dangerous practice:
{                                           // inline accessors
	return d_size;
}

#endif
