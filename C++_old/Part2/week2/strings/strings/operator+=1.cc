#include "strings.ih"
// Instead of adding a string, promote the string to a Strings object 
void Strings::operator+=(string const &right)
{
	add(right);
}
