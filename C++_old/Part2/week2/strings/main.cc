#include "main.ih"

extern char **environ ;

void fun(Strings const &strings)
{
	cout << strings[1] << '\n';
}

int main(int argc, char **argv)
{
	Strings strings(argc, argv);

	strings[0] = "hello world";
	strings[1][0] = 'H';

	cout << strings[1] << '\n';
	fun(strings);

	Strings str(argc, argv);

	str += "+= toegevoegd";
	cout << str[argc] << '\n';

	Strings env(environ);

	Strings combi(str + strings);
	cout << combi[argc] << '\n';

	str += env;

	Strings c1(Strings(env) + env);
	Strings c2(env + Strings(env));
	Strings c3(Strings(env) + Strings(env));

}
