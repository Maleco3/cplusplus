#include "main.ih"

int main(int argc, char **argv)
{
	// Copy elision
	// 	Instead of using the Copy Constructor
	// 	Copy elision is performed 
	Demo dem;
	Demo dem2 = dem.factory();
	// Outputs:
	// "Constructor"
	// "Constructor"
	
	Demo dem3(std::move(dem2));
	// Outputs:
	// "Move Constructor"

	cout << "\nUsing Copy Assignment\n";
	Demo dem4 = move(dem.factory());
	
}
