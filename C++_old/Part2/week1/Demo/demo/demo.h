#ifndef INCLUDED_DEMO_
#define INCLUDED_DEMO_

#include <iostream>

class Demo
{
	public:
		Demo();	// 1 constructor
		Demo(Demo const &other);// 2 copy constructor
		Demo(Demo &&tmp);// 3 move constructor
		~Demo();// destructor

		Demo &operator=(Demo const &rvalue); // copy assignment
		Demo &operator=(Demo &&tmp); // move assignment

		Demo factory();

	private:
};

inline Demo::Demo() 
{
	std::cout << "Constructor\n";
}

inline Demo::Demo(Demo const &other) 
{
	std::cout << "Copy Constructor\n";
}

inline Demo::Demo(Demo &&tmp) 
{
	std::cout << "Move Constructor\n";
}

inline Demo::~Demo()
{
	std::cout << "Destructor\n";
}

inline Demo Demo::factory()
{
	Demo ret;
	return ret;
}

inline Demo &Demo::operator=(Demo const &rvalue)
{
	std::cout << "Copy Assignment\n";
}

inline Demo &Demo::operator=(Demo &&rvalue)
{
	std::cout << "Move Assignment\n";
}
#endif
