#include "main.ih"

extern char **environ;

int main(int argc, char **argv)
{
    Strings args(argc, argv);
    Strings env(environ);

    if (env.size() >= 5)
        args.add(env.at(4));

    for (size_t idx = 0, end = args.size(); idx != end; ++idx)
        cout << args.at(idx) << '\n';
}
