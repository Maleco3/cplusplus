#include "strings.ih"

Strings::Strings(int argc, char *argv[])
	:
		d_str(0),
		d_size(0)
{
	for (size_t begin = 0, end = argc; begin != end; ++begin)
		add(argv[begin]);
}
