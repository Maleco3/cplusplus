#include "strings.ih"

void Strings::add(string const &next)
{
    string *tmp = duplicateAndEnlarge();

    tmp[d_size] = next;

    destroy();

    d_str = tmp;
    ++d_size;
}
