#ifndef INCLUDED_STRINGS_
#define INCLUDED_STRINGS_

#include <iosfwd>

class Strings
{
	std::string *d_str;
	size_t d_size;

	public:
	Strings();	// 1
	Strings(int argc, char *argv[]);	// 2
	Strings(char *environLike[]);	// 3
	Strings(Strings const &other);	// 4 copy constructor
	Strings(Strings &&tmp);	// 5 move constructor
	~Strings();
	Strings & operator=(Strings const &rvalue);	// 1 assignment operator 
	Strings & operator=(Strings &&rvalue);// 2 move operator


	void swap(Strings &other);
	size_t size() const;
	std::string const &at(size_t idx) const;    // for const-objects
	std::string &at(size_t idx);                // for non-const objects

	void add(std::string const &next);          // add another element

	private:
	std::string &safeAt(size_t idx) const;      // private backdoor
	std::string *duplicateAndEnlarge();
	void destroy();                 
};

inline size_t Strings::size() const         // potentially dangerous practice:
{                                           // inline accessors
	return d_size;
}

inline std::string const &Strings::at(size_t idx) const
{
	return safeAt(idx);
}

inline std::string &Strings::at(size_t idx)
{
	return safeAt(idx);
}


#endif

