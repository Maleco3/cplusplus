#include<iostream>
namespace First {
	enum Enum {SYMBOL};
	void fun(First::Enum symbol)
	{
		std::cout << "First\n";
	}
}
namespace Second {
	void fun(First::Enum symbol) 
	{
		std::cout << "Second\n";
	}
}

int main () 
{
	fun(First::Enum::SYMBOL);
	// Output: "First"
	Second::fun(First::Enum::SYMBOL);
	// Output: "Second"
}
